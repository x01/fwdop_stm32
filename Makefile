#export PATH=../arm-tools/_build/_toolchain/bin:${PATH}
#sudo cu -s 115200 -l /dev/tty.SLAB_USBtoUART
#sudo cu -s 57600 -l /dev/tty.SLAB_USBtoUART
# ~. to quit

NAME=fwdop
INC=-Iexternal/cmsis/CMSIS/Core/Include -Iexternal/cmsis_device_f0/Include -Iexternal/CMSIS_5/Device/ARM/ARMCM0/Include
CCFLAGS=-Os -g -mcpu=cortex-m0 -march=armv6-m -fno-asynchronous-unwind-tables $(INC) \
		--specs=nosys.specs 
		
		
		#-nostdlib
		
#  \
# 	   	-nostartfiles -nostdlib -nodefaultlibs -nolibc

CPPFLAGS=-std=c++17 -fno-exceptions $(CCFLAGS)
CFLAGS=$(CCFLAGS)
CPPC=arm-none-eabi-g++ $(CPPFLAGS) -c
CC=arm-none-eabi-gcc $(CFLAGS) -c
AS=arm-none-eabi-as -mcpu=cortex-m0 -mthumb --gdwarf2 
LINK=arm-none-eabi-gcc $(CCFLAGS)
SRCS:=$(wildcard source/*.cpp) $(wildcard source/*.c) $(wildcard source/*.h) $(wildcard source/*.hpp) $(wildcard source/*.S)
OBJS:=$(patsubst %.c, %.o, $(patsubst %.cpp, %.o, $(patsubst %.S, %.o, $(SRCS))))
HEADERS:=$(filter %.h, $(SRCS)) $(filter %.hpp, $(SRCS))
OBJS:=$(filter %.o, $(OBJS))
OBJS:=$(OBJS:source/%=derived/%)
DEPS:=$(OBJS:%.o=%.d)

#LDSCRIPT=external/CMSIS_5/Device/ARM/ARMCM0/Source/GCC/gcc_arm.ld
LDSCRIPT=source/stm32f030.ld
#STARTUP=external/CMSIS_5/Device/ARM/ARMCM0/Source/startup_ARMCM0.c external/CMSIS_5/Device/ARM/ARMCM0/Source/system_ARMCM0.c

$(info $(HEADERS))

derived/$(NAME).bin: derived/$(NAME).elf
	arm-none-eabi-objcopy -S -O binary $< $@
	
derived/$(NAME).ihex: derived/$(NAME).elf
	arm-none-eabi-objcopy -O ihex $< $@

derived/$(NAME).elf: $(OBJS) $(LDSCRIPT)
	$(LINK) $(filter %.o, $^) $(STARTUP) -T $(filter %.ld, $^) -Xlinker -Map=derived/$(NAME).map -o $@ 
	arm-none-eabi-size $@

derived/%.o : source/%.cpp $(HEADERS) | derived
	$(CPPC) $< -o $@

derived/%.o : source/%.c $(HEADERS) | derived
	$(CC) $< -o $@

derived/%.o : source/%.S | derived
	$(AS) $< -o $@

derived:
	mkdir -p derived

.PHONY: clean
clean:
	rm -rf derived/*.o derived/*.bin derived/*.elf derived/*.map derived/*.ihex
	rm -rf derived/fwdop_tests*
	rm -f derived/$(NAME)
	rmdir derived

flash: all
	./launch_openocd.sh flash

ocd: flash
	./launch_openocd.sh openocd

all: derived/$(NAME).bin derived/$(NAME).ihex
