cxx.std=c++14
using cxx

config.import.doctest = lib/doctest

import libs = doctest%lib{doctest}

hxx{*}: extension = h
cxx{*}: extension = cpp

derived/exe{fwdop_tests}: source/cxx{parser printf} tests/{hxx cxx}{**} $libs


