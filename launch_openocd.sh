#/bin/bash

BIN=derived/fwdop.bin

# based on this helpful guide
# https://github.com/mystorm-org/BlackIce-II/wiki/Using-an-STLink-Dongle-and-GDB-with-your-BlackIce-II

export PATH=../arm-tools/_build/_toolchain/bin:${PATH}


case $1 in
	flash)
		st-flash write ${BIN} 0x08000000
		;;
	openocd)
		ocd=`which openocd`
		link=`readlink ${ocd}`
		[[ $? == 0 ]] && ocd=${ocd%/*}/${link}

		share="${ocd%/*}/../share"
		#echo "share: ${share}"
		openocd -f ${share}/openocd/scripts/interface/stlink-v2.cfg -f ${share}/openocd/scripts/target/stm32f0x.cfg
		exit
		;;
	telnet)
		telnet localhost 4444
		exit
		;;
	gdb)
		arm-none-eabi-gdb -ex "target remote localhost:3333" "${BIN%.*}.elf"
		exit
		;;
	*)
		echo "launch flash|openocd|telnet|gdb"
		echo "To actually debug you want to launch this with 'openocd' to start openocd service. Then in another window"
		echo "launch 'telnet' option and issue a 'reset halt' command. It will reset the device and suspend execution."
		echo "Exit telnet and do 'gdb' to finally start debugging in gdb."
		exit
		;;
esac

