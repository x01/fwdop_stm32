#ifdef __arm__
#include "stm32f030x6.h"
#define CLOCK_MHZ 8
#define CLOCK_KHZ (CLOCK_MHZ * 1000)
#define CLOCK_HZ (CLOCK_MHZ * 1000 * 1000)
#endif

