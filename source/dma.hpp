#ifndef DMA_INCLUDED_FSwhWhdK_H_
#define DMA_INCLUDED_FSwhWhdK_H_

#include <stm32f030x6.h>

#include <cstddef>
#include "utils.h"

namespace stm32 { namespace p {

// This register is used for specific configurations of memory and DMA requests remap and to control special I/O features.
// Two bits are used to configure the type of memory accessible at address 0x0000 0000. These bits are used to select the physical remap by software and so, bypass the hardware BOOT selection.
// After reset these bits take the value selected by the actual boot mode configuration. Address offset: 0x00
// Reset value: 0x0000 000X (X is the memory mode selected by the actual boot mode configuration
    struct syscfg
    {
        // USART3_DMA_RMP:USART3 DMA request remapping bit. Available on STM32F070xB devices only.
        // This bit is set and cleared by software. It controls the remapping of USART3 DMA requests.
        // 0: Disabled, need to enable remap before use.
        // 1: Remap (USART3_RX and USART3_TX DMA requests mapped on DMA channel 3 and 2 respectively)

        // I2C_PAx_FMP:FastModePlus(FM+)driving capability activation bits. Available on STM32F030x4, STM32F030x6, STM32F070x6 and STM32F030xC devices only.
        // These bits are set and cleared by software. Each bit enables I2C FM+ mode for PA10 and PA9 I/Os.
        // 0: PAx pin operates in standard mode.
        // 1: I2C FM+ mode enabled on PAx pin and the Speed control is bypassed.

        // I2C1_FMP:FM+driving capability activation for I2C1. Not available on STM32F030x8 devices. This bit is set and cleared by software. This bit is OR-ed with I2C_Pxx_FM+ bits.
        // 0: FM+ mode is controlled by I2C_Pxx_FM+ bits only.
        // 1: FM+ mode is enabled on all I2C1 pins selected through selection bits in GPIOx_AFR registers. This is the only way to enable the FM+ mode for pads without a dedicated I2C_Pxx_FM+ control bit.

        // I2C_PBx_FMP:FastModePlus(FM+) driving capability activation bits.
        // These bits are set and cleared by software. Each bit enables I2C FM+ mode for PB6, PB7, PB8, and PB9 I/Os.
        // 0: PBx pin operates in standard mode.
        // 1: I2C FM+ mode enabled on PBx pin and the Speed control is bypassed.

        // TIM17_DMA_RMP:TIM17 DMA request remapping bit. Available on STM32F030x4, STM32F030x6, STM32F070x6, STM32F030x8 and STM32F070xB devices only.
        // This bit is set and cleared by software. It controls the remapping of TIM17 DMA requests.
        // 0: No remap (TIM17_CH1 and TIM17_UP DMA requests mapped on DMA channel 1) 1: Remap (TIM17_CH1 and TIM17_UP DMA requests mapped on DMA channel 2)

        // TIM16_DMA_RMP:TIM16 DMA request remapping bit. Available on STM32F030x4, STM32F030x6, STM32F070x6, STM32F030x8 and STM32F070xB devices only.
        // This bit is set and cleared by software. It controls the remapping of TIM16 DMA requests.
        // 0: No remap (TIM16_CH1 and TIM16_UP DMA requests mapped on DMA channel 3) 1: Remap (TIM16_CH1 and TIM16_UP DMA requests mapped on DMA channel 4)

        // USART1_RX_DMA_RMP:USART1_RX DMA request remapping bit. Available on STM32F030x4, STM32F030x6, STM32F070x6, STM32F030x8 and STM32F070xB devices only.
        // This bit is set and cleared by software. It controls the remapping of USART1_RX DMA requests.
        // 0: No remap (USART1_RX DMA request mapped on DMA channel 3)
        // 1: Remap (USART1_RX DMA request mapped on DMA channel 5)
        // USART1_TX_DMA_RMP:USART1_TX DMA request remapping bit.. Available on STM32F030x4,STM32F030x6, STM32F070x6, STM32F030x8 and STM32F070xB devices only.
        // This bit is set and cleared by software. It bit controls the remapping of USART1_TX DMA requests.
        // 0: No remap (USART1_TX DMA request mapped on DMA channel 2)
        // 1: Remap (USART1_TX DMA request mapped on DMA channel 4)
        static void remap_usart1(bool rx_3_to_5, bool tx_2_to_4) {
            uint32_t r = SYSCFG->CFGR1;
            if (rx_3_to_5) r |= SYSCFG_CFGR1_USART1RX_DMA_RMP; else r &= SYSCFG_CFGR1_USART1RX_DMA_RMP;
            if (tx_2_to_4) r |= SYSCFG_CFGR1_USART1TX_DMA_RMP; else r &= SYSCFG_CFGR1_USART1TX_DMA_RMP;
        }

        // ADC_DMA_RMP:ADC DMA request remapping bit. Availableon STM32F030x4,STM32F030x6, STM32F070x6, STM32F030x8 and STM32F070xB devices only.
        // This bit is set and cleared by software. It controls the remapping of ADC DMA requests. 0: No remap (ADC DMA request mapped on DMA channel 1)
        // 1: Remap (ADC DMA request mapped on DMA channel 2)

        // PA11_PA12_RMP:PA11 and PA12 remapping bit for small packages (28 and 20pins). Available on STM32F070x6 devices only.
        // This bit is set and cleared by software. It controls the mapping of either PA9/10 or PA11/12 pin pair on small pin-count packages.
        // 0: No remap (pin pair PA9/10 mapped on the pins)
        // 1: Remap (pin pair PA11/12 mapped instead of PA9/10)

        // MEM_MODE[1:0]:Memory mapping selection bits
        // These bits are set and cleared by software. They control the memory internal mapping at address 0x0000 0000. After reset these bits take on the value selected by the actual boot mode configuration. Refer to Chapter 2.5: Boot configuration for more details.
        // x0: Main Flash memory mapped at 0x0000 0000
        // 01: System Flash memory mapped at 0x0000 0000
        // 11: Embedded SRAM mapped at 0x0000 0000
    };

    struct dma
    {
        DMA_TypeDef* base;
        enum class intr : uint32_t {
            global=DMA_ISR_GIF1, complete=DMA_ISR_TCIF1, half=DMA_ISR_HTIF1, err=DMA_ISR_TEIF1,
            all=DMA_ISR_GIF1|DMA_ISR_TCIF1|DMA_ISR_HTIF1|DMA_ISR_TEIF1
        };
        enum chan : uint32_t { ch1 = 0, ch2 = 1, ch3 = 2, ch4 = 3, ch5 = 4 };  

        constexpr dma(DMA_TypeDef* base) : base(base) {}

        bool error(chan channel) const { return base->ISR & (DMA_ISR_TEIF1 << 4*channel); }
        bool half(chan channel) const { return base->ISR & (DMA_ISR_HTIF1 << 4*channel); }
        bool complete(chan channel) const { return base->ISR & (DMA_ISR_TCIF1 << 4*channel); }
        bool global(chan channel) const { return base->ISR & (DMA_ISR_GIF1 << 4*channel); }

        bool is_set(chan channel, dma::intr flags) { return base->ISR & (uint32_t(flags) << 4*channel); }
        void clear(chan channel, dma::intr flags) { base->IFCR |= uint32_t(flags) << 4*channel; }
    };
    ENUM_FLAGS(dma::intr)

    struct dma_chan
    {
        DMA_Channel_TypeDef* base;

        dma_chan() {}
        constexpr dma_chan(DMA_Channel_TypeDef* base) : base(base) {}
        constexpr dma_chan(dma::chan ch) : base(
            ch==dma::ch1 ? DMA1_Channel1 :
            ch==dma::ch2 ? DMA1_Channel2 :
            ch==dma::ch3 ? DMA1_Channel3 :
            ch==dma::ch4 ? DMA1_Channel4 : DMA1_Channel5 ) {}

        // enable/disable DMA channel
        void enable(bool enable) {
            assert((RCC->AHBENR & RCC_AHBENR_DMAEN) != 0);
            if (enable) base->CCR |= DMA_CCR_EN; else base->CCR &= ~DMA_CCR_EN;
        }
        
        // enable interrupt
        enum intre : uint32_t { none = 0, tc = DMA_CCR_TCIE, ht = DMA_CCR_HTIE, err = DMA_CCR_TEIE, all = DMA_CCR_TCIE|DMA_CCR_HTIE|DMA_CCR_TEIE};
        void ie(intre e, intre disable = intre::none) { base->CCR = base->CCR & ~disable | e; }

        // 0: Read from peripheral
        // 1: Read from memory
        enum class dir : unsigned { from_peripheral = 0, from_memory = 1 };
        void direction(dir d) { if (d == dir::from_peripheral) base->CCR &= ~DMA_CCR_DIR; else base->CCR |= DMA_CCR_DIR; }
        void circular(bool enable) { if (enable) base->CCR |= BV(DMA_CCR_CIRC, 1); else base->CCR &= ~BV(DMA_CCR_CIRC, 1); }
        void inc_peripheral(bool enable) { if (enable) base->CCR |= BV(DMA_CCR_PINC, 1); else base->CCR &= ~BV(DMA_CCR_PINC, 1); }
        void inc_memory(bool enable) { if (enable) base->CCR |= BV(DMA_CCR_MINC, 1); else base->CCR &= ~BV(DMA_CCR_MINC, 1); }
        
        enum class size : uint32_t { bit8=0, bit16, bit32 };
        void peripheral_size(size sz) { base->CCR = (base->CCR & ~DMA_CCR_PSIZE_Msk) | BV(DMA_CCR_PSIZE, sz); }
        void memory_size(size sz) { base->CCR = (base->CCR & ~DMA_CCR_MSIZE_Msk) | BV(DMA_CCR_MSIZE, sz); }

        enum class prio : uint32_t { low, med, high, very_high };
        void priority(prio p) { base->CCR = (base->CCR & ~DMA_CCR_PL_Msk) | BV(DMA_CCR_PL, p); }
        void mem2mem(bool enable) { if (enable) base->CCR |= BV(DMA_CCR_MEM2MEM, 1); else base->CCR &= ~BV(DMA_CCR_MEM2MEM, 1); }

        // NDT[15:0]:Number of data to transfer
        // Number of data to be transferred (0 up to 65535). This register can only be written when the channel is disabled. Once the channel is enabled, this register is read-only, indicating the remaining bytes to be transmitted. This register decrements after each DMA transfer.
        // Once the transfer is completed, this register can either stay at zero or be reloaded automatically by the value previously programmed if the channel is configured in circular mode.
        // If this register is zero, no transaction can be served whether the channel is enabled or not.
        uint16_t tx_size() const { return (base->CNDTR & DMA_CNDTR_NDT_Msk) >> DMA_CNDTR_NDT_Pos; }
        void tx_size(uint16_t sz) { base->CNDTR = base->CNDTR & ~DMA_CNDTR_NDT_Msk | sz; }

        // PA[31:0]:Peripheral address
        // Base address of the peripheral data register from/to which the data will be read/written.
        // When PSIZE is 01 (16-bit), the PA[0] bit is ignored. Access is automatically aligned to a half- word address.
        // When PSIZE is 10 (32-bit), PA[1:0] are ignored. Access is automatically aligned to a word address.
        void* peripheral_addr() const { return (void*)base->CPAR; }
        void peripheral_addr(void* addr) { assert((base->CCR & DMA_CCR_EN) == 0); base->CPAR = (uint32_t)addr; }

        // MA[31:0]:Memoryaddress
        // Base address of the memory area from/to which the data will be read/written.
        // When MSIZE is 01 (16-bit), the MA[0] bit is ignored. Access is automatically aligned to a half- word address.
        // When MSIZE is 10 (32-bit), MA[1:0] are ignored. Access is automatically aligned to a word address.
        void* memory_addr() const { return (void*)base->CMAR; }
        void memory_addr(void* addr) { base->CMAR = (uint32_t)addr; }

        void config(dir dir_, bool circular,
            size periph_size, bool inc_periph,
            size memory_size, bool inc_memory,
            prio prio_)
        {
            uint32_t ccr = base->CCR;
            ccr &= ~DMA_CCR_DIR;    ccr |= BV(DMA_CCR_DIR, (uint32_t)dir_);
            ccr &= ~DMA_CCR_CIRC;   ccr |= BV(DMA_CCR_CIRC, (uint32_t)circular);

            ccr &= ~DMA_CCR_PINC;   ccr |= BV(DMA_CCR_PINC, (uint32_t)inc_periph);
            ccr &= ~DMA_CCR_PSIZE;  ccr |= BV(DMA_CCR_PSIZE, (uint32_t)periph_size);

            ccr &= ~DMA_CCR_MINC;   ccr |= BV(DMA_CCR_MINC, (uint32_t)inc_memory);
            ccr &= ~DMA_CCR_MSIZE;  ccr |= BV(DMA_CCR_MSIZE, (uint32_t)memory_size);

            ccr &= ~DMA_CCR_PL;     ccr |= BV(DMA_CCR_PL, (uint32_t)prio_);
            base->CCR = ccr;
        }
    };

    ENUM_FLAGS(dma_chan::intre)

}}

#endif
