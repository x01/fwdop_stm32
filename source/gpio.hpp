#ifndef GPIO_INCLUDED_FSwhWhdK_H_
#define GPIO_INCLUDED_FSwhWhdK_H_
#pragma once

#include <stm32f030x6.h>

#include <cstddef>
#include "utils.h"

namespace stm32 { namespace p {

struct gpio
{
    GPIO_TypeDef* base;

    gpio(GPIO_TypeDef* p) : base(p) {}

    enum class pin_mode : uint32_t { in, out, alternate, analog };
    void mode(unsigned pin, pin_mode mod) {
        base->MODER = (base->MODER & ~(GPIO_MODER_MODER0 << pin*2)) | ((uint32_t)mod << pin*2);
    }

    enum class otype { push_pull = 0, open_drain = 1 };
    void output(unsigned pin, otype type) { base->OTYPER = base->OTYPER & ~(GPIO_OTYPER_OT_0 << pin) | ((uint32_t)type << pin); }

    enum class pin_speed { low = 0, med = 1, high = 3 };
    void speed(unsigned pin, pin_speed spd) { base->OSPEEDR = base->OSPEEDR & ~(GPIO_OSPEEDER_OSPEEDR0 << pin*2) | ((uint32_t)spd << pin*2); }

    enum class pupd { no_pull, pull_up, pull_down };
    void pull(unsigned pin, pupd pupd) { base->PUPDR = base->PUPDR & ~(GPIO_PUPDR_PUPDR0 << pin*2) | ((uint32_t)pupd << pin*2); }

    void pin(unsigned pin, pin_mode mod, otype pushpull_opendrain, pin_speed spd, pupd pupd)
    {
        mode(pin, mod);
        output(pin, pushpull_opendrain);
        speed(pin, spd);
        pull(pin, pupd);
    }

    bool input(unsigned pin) { return base->IDR & (1U<<pin); }
    bool output(unsigned pin) { return (base->ODR & (1U<<pin)) >> pin; }
    void output(unsigned pin, bool value) { if (value) base->ODR |= (1U<<pin); else base->ODR &= ~(1U<<pin); }
    void set(unsigned pin) { base->BSRR |= (1U<<pin); }
    void reset(unsigned pin) { base->BSRR |= (0x10000U<<pin); }

    // lock

    // AFSELy[3:0]:Alternate function selection for portx piny(y=0..7)
    // These bits are written by software to configure alternate function I/Os
    // 0000: AF0
    // 0001: AF1
    // 0010: AF2
    // 0011: AF3
    // 0100: AF4
    // 0101: AF5
    // 0110: AF6
    // 0111: AF7
    // 1000 -- 1111: Reserved
    void alternate(unsigned pin, unsigned func)
    {
        uint32_t reg = pin >> 3;
        assert(reg <= 1);
        uint32_t pos = (pin & 7) * 4;
        assert(pos <= 32-4);
        base->AFR[reg] = base->AFR[reg] & ~(0xf << pos) | (func << pos);
    }

    void reset_output(unsigned pin) { base->BRR |= (1<<pin); } 
};

}}

#endif