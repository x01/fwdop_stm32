#include "i2c.hpp"
#include "dma.hpp"
#include <utility>
#include <cstring>

#define LOG_INT 0
#define LOG_CMD 0
#define LOG_CMD_DMA 0

#define LOG_IS_ON (LOG_INT || LOG_CMD || LOG_CMD_DMA)

#if LOG_IS_ON
unsigned send_id = 0;
#endif

// docs:
// http://www.ti.com/lit/an/slva704/slva704.pdf
// i2c start condition: SCL HI, SDA HI->LO
// i2c stop condition:	SCL HI, SDA LO->HI
// ACK:					SDA=0 while SCL LO->HI (noACK SDA=1). SDA is set by receiving end
// DATA:				SDA=data while SCL LO->HI
i2c_com g_i2c;

extern "C" void i2c1_handler()
{
    // using namespace stm32::p;
    // i2c iic {I2C1};
    // dbg_printf("#%d I2C ISR[autoend:%d, reload:%d] isr[txis:%d tc:%d tcr:%d tex:%d]\r\n", send_id,
    //     iic.autoend(), iic.reload(),
    //     iic.isr_isset(i2c::isr_flag::txis), iic.isr_isset(i2c::isr_flag::tc), iic.isr_isset(i2c::isr_flag::tcr),
    //     iic.isr_isset(i2c::isr_flag::txe)
    // );
    g_i2c.internal_irq();
}

extern "C" void dma_ch2_3_handler()
{
//    g_i2c.internal_dma_interrupt();
}

void i2c_com::issue_dma_transfer(void const* data, size_t size)
{
    using namespace stm32::p;

    i2c iic { I2C1 };
    stm32::p::dma d{ DMA1 };
    dma_chan dtx { i2c_com::tx_dma_ch };

    iic.txdma(true);
    
    dtx.enable(false);
    dtx.config(dma_chan::dir::from_memory, true,
        dma_chan::size::bit8, false,
        dma_chan::size::bit8, true,
        dma_chan::prio::med);
    dtx.ie(dma_chan::intre::tc|dma_chan::intre::err);
    dtx.tx_size(size);
    dtx.peripheral_addr((char*)iic.tx_addr());
    dtx.memory_addr((void*)data);
//    nvic::enable(true, DMA1_Channel2_3_IRQn, 0);
    dtx.enable(true);
}

void i2c_com::start_sending(bool cont)
{
    using namespace stm32::p;
    i2c iic {I2C1};
    auto [reload, toSend] = calc_n(m_to_send);

    bool is_nostop = (m_flags & sendfl::nostop);
    bool is_seq = (m_flags & sendfl::seq);
    bool is_dma = (m_flags & sendfl::dma);

#if LOG_CMD
    dbg_printf(col_blue "#%d SEND, %d/%d ask-reload:%d nostop:%d cont:%d seq:%d dma:%d\r\n" col_x, send_id,
        toSend, m_to_send, reload, is_nostop, cont, is_seq, is_dma);
#endif

    // Always need reload if sequence flag is set
    if (is_seq) reload = true;

    __disable_irq();
    nvic::enable(true, I2C1_IRQn, 1);
    iic.num_bytes(toSend);
    iic.autoend(!reload && !is_nostop); // only matters if reload == 0
    iic.reload(reload);
    iic.ie(i2c::intr::tx|i2c::intr::tc|i2c::intr::error|i2c::intr::nack);

    if (is_dma) {
        issue_dma_transfer(m_data, toSend);
        if ((m_to_send -= toSend) == 0)
            m_data = nullptr;
        else
            m_data += toSend;
    } else {
        iic.txdma(false);
    }

    // In case we're continuing, no need to generate start condition
    if (!cont)
        iic.start();
    __enable_irq();
}

void i2c_com::internal_irq() {

    using namespace stm32::p;
    i2c iic {I2C1};

    uint32_t isr = iic.isr();

    if (isr & (uint32_t)i2c::isr_flag::any_error) {
#if LOG_INT >= 1
        dbg_printf("#%d i ERR: I2C_ERROR:%8x\r\n", send_id, isr & (uint32_t)i2c::isr_flag::any_error);
#endif
        m_data = nullptr;
        m_to_send = 0;
        return;
    }

    if (isr & (uint32_t)i2c::isr_flag::nackf) {
#if LOG_INT >= 1
        dbg_printf("#%d i  NACK: I2C_NACKF:%8x\r\n", send_id, isr & (uint32_t)i2c::isr_flag::any_error);
#endif
        m_data = nullptr;
        m_to_send = 0;
        return;
    }

    if (isr & (uint32_t)i2c::isr_flag::txis) {
        assert(m_to_send != 0);

        m_to_send--;
#if LOG_INT >= 2
        dbg_printf("#%d i TX: %02x to-send:%3d [isr-txis:%d isr-tc:%d isr-tcr:%d]\r\n", send_id, *m_data, m_to_send,
            iic.isr_isset(i2c::isr_flag::txis), iic.isr_isset(i2c::isr_flag::tc), iic.isr_isset(i2c::isr_flag::tcr));
#endif
        iic.tx(*m_data++);

        if (m_to_send == 0) {
            m_data = nullptr;
#if LOG_INT >= 1
            dbg_printf("#%d i TX: DONE\r\n", send_id);
#endif
        }
        return;
    }

// When RELOAD=0 and NBYTES data have been transferred:
// – In automatic end mode (AUTOEND=1), a STOP is automatically sent.
// – In software end mode (AUTOEND=0), the TC flag is set and the SCL line is stretched low in order to perform software actions:
// A RESTART condition can be requested by setting the START bit in the I2C_CR2 register with the proper slave address configuration, and number of bytes to be transferred. Setting the START bit clears the TC flag and the START condition is sent on the bus.
// A STOP condition can be requested by setting the STOP bit in the I2C_CR2 register. Setting the STOP bit clears the TC flag and the STOP condition is sent on the bus.    

    if (isr & (uint32_t)i2c::isr_flag::tcr) {
        auto [reload, toSend] = calc_n(m_to_send);
#if LOG_INT >= 1
        dbg_printf("#%d i TCR: I2C RELOAD[autoend:%d, reload:%d], %d/%d will-reload:%d [isr-txis:%d isr-tc:%d isr-tcr:%d]\r\n", send_id,
            iic.autoend(), iic.reload(),
            toSend, m_to_send, reload,
            iic.isr_isset(i2c::isr_flag::txis), iic.isr_isset(i2c::isr_flag::tc), iic.isr_isset(i2c::isr_flag::tcr)
        );
#endif
        // This means we've sent all we had, now the next "send" will provide more data.
        // But we don't want to get ISR called immediatly again, so disable TransferCompleInterrupt and 
        // wait for more data.
        if (m_to_send == 0) {
            m_data = nullptr;
            assert(m_flags & (sendfl::seq|sendfl::dma));
            iic.ie(i2c::intr::none, i2c::intr::tc);
#if LOG_INT >= 1
            dbg_printf("#%d i TCR: I2C disable TCI, wait for more data to send\r\n", send_id);
#endif
        } else {
            iic.num_bytes(toSend);
            iic.reload(reload);
            if (!reload)
                iic.autoend((m_flags & sendfl::nostop) == 0);

            // setup dma
            if (m_flags & sendfl::dma) {
                issue_dma_transfer(m_data, toSend);

                if ((m_to_send -= toSend) == 0)
                    m_data = nullptr;
                else
                    m_data += toSend;
            }

        }
#if LOG_INT >= 1
        dbg_printf("#%d i  TCR: autoend:%d reload:%d num-bytes:%d nostop:%d\r\n", send_id, iic.autoend(), iic.reload(), iic.num_bytes(), bool(m_flags&sendfl::nostop));
#endif
    }

    // set when nbytes was sent and RELOAD=0, AUTOEND=0
    if (isr & (uint32_t)i2c::isr_flag::tc) {
#if LOG_INT >= 1
        dbg_printf("#%d i  TC: I2C TRANSFER COMPLETE [autoend:%d reload:%d num-bytes:%d], to-send:%d nostop:%d seq:%d isr:%8x[txs:%d tc:%d tcr:%d]\r\n",
            send_id, iic.autoend(), iic.reload(), iic.num_bytes(),
            m_to_send, bool(m_flags&sendfl::nostop), bool(m_flags&sendfl::seq),
            iic.isr(), iic.isr_isset(i2c::isr_flag::txis), iic.isr_isset(i2c::isr_flag::tc), iic.isr_isset(i2c::isr_flag::tcr));
#endif

        assert (m_to_send == 0);
        assert ((m_flags & sendfl::nostop) != 0);
        m_data = nullptr;

        // Disable interrupt not to re-enter ISR on this condition
        iic.ie(i2c::intr::none, i2c::intr::tc);

//        if ((m_flags & sendfl::softstop) == 0) {
// #if LOG_INT >= 1
//             dbg_printf("#%d i  STOP\r\n", send_id);
// #endif
//             iic.stop();
//        }
    }
}

void i2c_com::wait_finished() {
    uint16_t start = now_ms();
    while (!finished()) {
        if (now_ms() - start > 1000) {
#if LOG_CMD >= 1
            dbg_printf("#%d remains:%8x\r", send_id, remains());
#endif
            start = now_ms();
        }
        //__WFI();
    }
}

void i2c_com::send(uint8_t const* p, uint16_t count, sendfl fl) {

    //assert(finished());
    assert(m_data == nullptr);
    assert(m_to_send == 0);

#if LOG_IS_ON
    send_id++;
#endif

    m_data = p;
    m_to_send = count;

    bool hadSeq = m_flags & sendfl::seq;
    m_flags = fl;

    start_sending(hadSeq);

    if ((fl & sendfl::nowait) == 0) {
#if LOG_CMD >= 1
        unsigned start = now_ms();
        dbg_printf("#%d   wait_finished\r\n", send_id);
#endif
        wait_finished();
#if LOG_CMD >= 1
        dbg_printf("#%d   waited %d ms\r\n", send_id, now_ms() - start);
#endif
    }
}

// void dma_ch1_handler()
// {
// dbg_printf(col_blue "DMA 1\r\n");
// }

extern "C" void dma_ch4_5_handler()
{
    dbg_printf(col_blue "DMA 4-5\r\n");
}

void i2c_com::internal_dma_interrupt()
{
    using namespace stm32::p;

    stm32::p::dma d { DMA1 };
	dma_chan c { DMA1_Channel2 };

	auto const ch = dma::ch2;

#if LOG_INT >= 1
    dbg_printf(col_magenta "#%d DMA. ISR:%x, chan[g:%d err:%d h:%d cmpl:%d] tx:%d" col_x "\r\n", send_id,
         d.base->ISR, d.global(ch), d.error(ch), d.half(ch), d.complete(ch), c.tx_size()
    );
#endif
	d.clear(ch, dma::intr::all);
	c.enable(false);
	//c.ie(dma_chan::intre::none, dma_chan::intre::all);
    m_to_send -= c.tx_size();
    if (m_to_send == 0) {
#if LOG_INT >= 1
    dbg_printf(col_magenta "#%d  data -> nul" col_x "\r\n", send_id);
#endif
        m_data = nullptr;
    }
//    m_data = nullptr;
}

bool i2c_com::finished() const {
#if LOG_CMD >= 1
    dbg_printf("#%d    finished, to-send:%d m_flags:0x%0x isr:%08x\r\n", send_id, m_to_send, m_flags, I2C1->ISR);
#endif
    using namespace stm32::p;
    i2c iic {I2C1};
    uint32_t isr = iic.isr(), fl = m_flags;

    bool seq_finished = (fl&sendfl::seq) && (isr & I2C_ISR_TCR);
    bool all_finished = (fl&sendfl::nostop) && (isr & I2C_ISR_TC);
    bool is_dma = (fl&sendfl::dma);
    bool is_busy = isr & I2C_ISR_BUSY;
    return m_data == nullptr
        && (all_finished || seq_finished || !is_busy);
}


void i2c_com::stop() {
    using namespace stm32::p;
    i2c iic {I2C1};
    iic.stop();

    m_to_send = 0;
    m_data = nullptr;
    m_flags = 0;
}
