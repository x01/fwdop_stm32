#ifndef I2C_INCLUDED_FSwhWhdK_H_
#define I2C_INCLUDED_FSwhWhdK_H_
#pragma once

#include <stm32f030x6.h>

#include <cstddef>
#include <iterator>
#include <algorithm>
#include "macrodef.h"
#include "utils.h"
#include "nvic.hpp"
#include "printf.h"
#include "dma.hpp"

namespace stm32 {

/*
namespace stir
{
	struct i2c
	{
		union cr1 {
			struct {
				uint32_t pe:1, txie:1, rxie:1, addrie:1, nackie:1, stopie:1, tcie:1, errie:1,
						 ndf:4, anfoff:1, res_1:1, txdmaen:1, rxdmaen:1,
						 sbc:1, nostretch:1, res_2:1, gcen:1, smbhen:1, smbden:1, alerten:1, pecen:1, res_3:8;
			};
			uint32_t reg;
		};

		union cr2 {
			struct {
				uint32_t sadd:10, rd_wrn:1, add10:1, head10r:1, start:1, stop:1, nack:1,
						 nbytes:8, reload:24, autoend:25, pecbyte:26, res_1:5;
			};
			uint32_t reg;
		};

		union oar1 {
			struct {
				uint32_t oa1_1:1, oa1_2:7, oa1_3:2, oa1mode:1, res_1:4, oa1en:1,
						 res_2:16;
			};
			uint32_t reg;
		};

		union oar2 {
			struct {
				uint32_t res_1:1, oa2:7, oa2msk:3, res_2:4, oa2en:1, res_3:16;
			};
			uint32_t reg;
		};

		union timingr {
			struct {
				uint32_t scll:8, sclh:8, sdadel:4, scldel:4, res_1:4, presc:4;
			};
			uint32_t reg;
		};

		union timeoutr {
			struct {
				uint32_t	timeouta:12, tidle:1, res_1:2, timouten:1,
							timeoutb:12, res_1:3, texten:1;
			};
			uint32_t reg;
		};

		union isr {
			struct {
				uint32_t	txe:1, txis:1, rxne:1, addr:1, nackf:1, stopf:1, tc:1, tcr:1, berr:1, arlo:1, ovr:1, pecerr:1,
						   	timeout:1, alert:1, res_1:1, busy:1, dir:1, addcode:7, res_2:8;
			};
			uint32_t reg;
		};

		union icr {
			struct {
				uint32_t	res_1:3, addrcf:1, nackcr:1, stopcf:1, res_2:2, berrcf:1, arlocf:1, ovrcf:1, peccf:1, timoutcf:1, alertcf:1,
						   	res_2:2, res_3:16;
			};
			uint32_t reg;
		};

		union pecr {
			struct {
				uint32_t	pec:8, res_1:24;
			};
			uint32_t reg;
		};

		union rxdr {
			struct {
				uint32_t	rxdata:8, res_1:24;
			};
			uint32_t reg;
		};

		union txdr {
			struct {
				uint32_t	txdata:8, res_1:24;
			};
			uint32_t reg;
		};
	};

class 
{

};

}
*/    
    
#define IMPL_ENABLE_BIT(COND, REG, FLAG) if (COND) base->REG |= (FLAG); else base->REG &= ~(FLAG)

    namespace p {

struct i2c
{
    I2C_TypeDef* base;

    i2c() {}
    i2c(I2C_TypeDef* base) : base(base) {}

    void init()
    {
        #ifdef I2C2
        if (base == I2C1)
        #endif
        {
            RCC->APB1ENR &= ~RCC_APB1ENR_I2C1EN;
            RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
        }
        #ifdef I2C2
        else
        {
            RCC->APB1ENR &= ~RCC_APB1ENR_I2C2EN;
            RCC->APB1ENR |= RCC_APB1ENR_I2C2EN;
        }
        #endif
    }

    void deinit()
    {
        #ifdef I2C2
        if (base == I2C1)
        #endif
        {
            RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
            RCC->APB1ENR &= ~RCC_APB1ENR_I2C1EN;
        }
        #ifdef I2C2
        else
        {
            RCC->APB1ENR |= RCC_APB1ENR_I2C2EN;
            RCC->APB1ENR &= ~RCC_APB1ENR_I2C2EN;
        }
        #endif
    }

    void send_address(uint8_t addr, bool transmit = true)
    {
        addr <<= 1;
        if (transmit) addr &= uint8_t(-1) - 1; else addr |= 1;
        base->TXDR = addr;
    }

    void wait_tx_empty() {
        while (!(base->ISR & I2C_ISR_TXE))
            ;
    }
    void wait_nackf() {
        while (!(base->ISR & I2C_ISR_NACKF))
            ;
    }


    // CR1
    void reset(bool enable) { IMPL_ENABLE_BIT(enable, CR1, I2C_CR1_SWRST); }
    void pec(bool enable) { IMPL_ENABLE_BIT(enable, CR1, I2C_CR1_PECEN); }
    void alert(bool enable) { IMPL_ENABLE_BIT(enable, CR1, I2C_CR1_ALERTEN); }
    enum class i2cmode : unsigned { i2c = 0, smbus = 1};
    void i2c_mode(i2cmode mode = i2cmode::i2c) { IMPL_ENABLE_BIT((bool)mode, CR1, I2C_CR1_SMBDEN); }
    void general_call(bool enable) { IMPL_ENABLE_BIT(enable, CR1, I2C_CR1_GCEN); }
    void stretch_clock(bool enable) { IMPL_ENABLE_BIT(!enable, CR1, I2C_CR1_NOSTRETCH); }
    void slave_byte_control(bool enable) { IMPL_ENABLE_BIT(enable, CR1, I2C_CR1_SBC); }
    void analog_noise_disable(bool disable) { IMPL_ENABLE_BIT(disable, CR1, I2C_CR1_ANFOFF); }

    void digital_noise_filter(unsigned t2clk) { assert(t2clk < 15); base->CR1 = (base->CR1 & ~I2C_CR1_DNF_Msk) | (t2clk << I2C_CR1_DNF_Pos); }

    enum class intr {
         none = 0,

        // ERRIE:Errorinterruptsenable
        // 0: Error detection interrupts disabled
        // 1: Error detection interrupts enabled
        // Any of these errors generate an interrupt:
        //     Arbitration Loss (ARLO)
        //     Bus Error detection (BERR)
        //     Overrun/Underrun (OVR)
        //     Timeout detection (TIMEOUT)
        //     PEC error detection (PECERR)
        //     Alert pin event detection (ALERT)        
        error = I2C_CR1_ERRIE,

        // TCIE: Transfer Complete interrupt enable
        // 0: Transfer Complete interrupt disabled
        // 1: Transfer Complete interrupt enabled
        // Note: Any of these events will generate an interrupt: Transfer Complete (TC)
        // Transfer Complete Reload (TCR)
        tc = I2C_CR1_TCIE,

        // STOPIE: STOP detection Interrupt enable
        // 0: Stop detection (STOPF) interrupt disabled
        // 1: Stop detection (STOPF) interrupt enabled
        stop = I2C_CR1_STOPIE,

        // NACKIE:Not acknowledge received Interrupt enable
        // 0: Not acknowledge (NACKF) received interrupts disabled
        // 1: Not acknowledge (NACKF) received interrupts enabled
        nack = I2C_CR1_NACKIE,

        // ADDRIE:AddressmatchInterruptenable (slaveonly)
        // 0: Address match (ADDR) interrupts disabled
        // 1: Address match (ADDR) interrupts enabled
        addr = I2C_CR1_ADDRIE,

        // RXIE:RX Interrupt enable
        // 0: Receive (RXNE) interrupt disabled
        // 1: Receive (RXNE) interrupt enabled
        rx = I2C_CR1_RXIE,

        // TXIE:TX Interrupt enable
        // 0: Transmit (TXIS) interrupt disabled
        // 1: Transmit (TXIS) interrupt enabled
        tx = I2C_CR1_TXIE
    };
    void ie(intr enable, intr disable = intr::none) { base->CR1 = base->CR1 & ~uint32_t(disable) | uint32_t(enable); }
    void enable(bool en) { IMPL_ENABLE_BIT(en, CR1, I2C_CR1_PE); }
    void txdma(bool enable) { IMPL_ENABLE_BIT(enable, CR1, I2C_CR1_TXDMAEN); }
    void rxdma(bool enable) { IMPL_ENABLE_BIT(enable, CR1, I2C_CR1_RXDMAEN); }

    // CR2
    void set_pecbyte() { base->CR2 |= I2C_CR2_PECBYTE; }

    // This bit is set and cleared by software.
    // 0: software end mode: TC flag is set when NBYTES data are transferred, stretching SCL low.
    // 1: Automatic end mode: a STOP condition is automatically sent when NBYTES data are transferred.
    // Note: This bit has no effect in slave mode or when the RELOAD bit is set.
    void autoend(bool enable) { IMPL_ENABLE_BIT(enable, CR2, I2C_CR2_AUTOEND); }
    bool autoend() const { return bool(base->CR2 & I2C_CR2_AUTOEND); }

    // IMPORTANT: set reload after setting num_bytes!
    // This bit is set and cleared by software.
    // 0: The transfer is completed after the NBYTES data transfer (STOP or RESTART will follow).
    // 1: The transfer is not completed after the NBYTES data transfer (NBYTES will be reloaded). TCR flag is set when NBYTES data are transferred, stretching SCL low.
    void reload(bool enable) { IMPL_ENABLE_BIT(enable, CR2, I2C_CR2_RELOAD); }
    // void num_bytes(uint8_t count, bool reload) {
    //     base->CR2 = (base->CR2 & ~I2C_CR2_NBYTES_Msk & I2C_CR2_RELOAD_Msk)
    //             | ((uint32_t)count << I2C_CR2_NBYTES_Pos)
    //             | (reload ? I2C_CR2_RELOAD : 0);
    // }
    void reload_disable() { base->CR2 &= ~I2C_CR2_RELOAD; }
    bool reload() const { return bool(base->CR2 & I2C_CR2_RELOAD); }

    // IMPORTANT: set reload before setting reload!
    // The number of bytes to be transmitted/received is programmed there. This field is don’t care in slave mode with SBC=0.
    // Note: Changing these bits when the START bit is set is not allowed.
    void num_bytes(uint8_t count) { base->CR2 = (base->CR2 & ~I2C_CR2_NBYTES_Msk) | ((uint32_t)count << I2C_CR2_NBYTES_Pos); }
    uint8_t num_bytes() const { return (base->CR2 & I2C_CR2_NBYTES_Msk) >> I2C_CR2_NBYTES_Pos; }

    // NACK: NACK generation(slavemode)
    // The bit is set by software, cleared by hardware when the NACK is sent, or when a STOP condition or an Address matched is received, or when PE=0.
    // 0: an ACK is sent after current received byte.
    // 1: a NACK is sent after current received byte.
    // Note: Writing ‘0’ to this bit has no effect.
    // This bit is used in slave mode only: in master receiver mode, NACK is automatically generated after last byte preceding STOP or RESTART condition, whatever the NACK bit value.
    // When an overrun occurs in slave receiver NOSTRETCH mode, a NACK is automatically generated whatever the NACK bit value.
    // When hardware PEC checking is enabled (PECBYTE=1), the PEC acknowledge value does not depend on the NACK value.    
    void nack(bool enable) { if (enable) base->CR2 |= I2C_CR2_NACK; else base->CR2 &= ~I2C_CR2_NACK; }

    // STOP: Stop generation(mastermode)
    // The bit is set by software, cleared by hardware when a Stop condition is detected, or when PE = 0.
    // In Master Mode:
    // 0: No Stop generation.
    // 1: Stop generation after current byte transfer.
    // Note: Writing ‘0’ to this bit has no effect
    void stop(bool enable = true) { if (enable) base->CR2 |= I2C_CR2_STOP; else base->CR2 &= ~I2C_CR2_STOP; }

    // START: Start generation
    // This bit is set by software, and cleared by hardware after the Start followed by the address sequence is sent, by an arbitration loss, by a timeout error detection, or when PE = 0. It can also be cleared by software by writing ‘1’ to the ADDRCF bit in the I2C_ICR register.
    // 0: No Start generation.
    // 1: Restart/Start generation:
    // – If the I2C is already in master mode with AUTOEND = 0, setting this bit generates a Repeated Start condition when RELOAD=0, after the end of the NBYTES transfer.
    // – Otherwise setting this bit will generate a START condition once the bus is free.
    // Note: Writing ‘0’ to this bit has no effect.
    // The START bit can be set even if the bus is BUSY or I2C is in slave mode.
    // This bit has no effect when RELOAD is set. In 10-bit addressing mode, if a NACK is received on the first part of the address, the START bit is not cleared by hardware and the master will resend the address sequence, unless the START bit is cleared by software.
    void start(bool enable = true) { if(enable) base->CR2 |= I2C_CR2_START; else base->CR2 &= ~I2C_CR2_START; }

    // HEAD10R: 10-bit address header only read direction(master receiver mode)
    // 0: The master sends the complete 10 bit slave address read sequence: Start + 2 bytes 10bit address in write direction + Restart + 1st 7 bits of the 10 bit address in read direction.
    // 1: The master only sends the 1st 7 bits of the 10 bit address, followed by Read direction.
    // Note: Changing this bit when the START bit is set is not allowed.
    void header_10bit_only_read(bool enable) { IMPL_ENABLE_BIT(enable, CR2, I2C_CR2_HEAD10R); }

    // ADD10:10-bit addressing mode (mastermode)
    // 0: The master operates in 7-bit addressing mode,
    // 1: The master operates in 10-bit addressing mode
    // Note: Changing this bit when the START bit is set is not allowed.
    void addr_10bit(bool enable) { IMPL_ENABLE_BIT(enable, CR2, I2C_CR2_ADD10); }

    // RD_WRN:Transfer direction(mastermode)
    // 0: Master requests a write transfer.
    // 1: Master requests a read transfer.
    // Note: Changing this bit when the START bit is set is not allowed.
    // SADD[9:8]:Slave address
    // bit 9:8(mastermode)
    // In 7-bit addressing mode (ADD10 = 0):
    // These bits are don’t care
    // In 10-bit addressing mode (ADD10 = 1):
    // These bits should be written with bits 9:8 of the slave address to be sent
    // Note: Changing these bits when the START bit is set is not allowed.
    // SADD[7:1]:Slaveaddressbit7:1(mastermode)
    // In 7-bit addressing mode (ADD10 = 0):
    // These bits should be written with the 7-bit slave address to be sent
    // In 10-bit addressing mode (ADD10 = 1):
    // These bits should be written with bits 7:1 of the slave address to be sent.
    // Note: Changing these bits when the START bit is set is not allowed.
    // SADD0:Slave address bit0(mastermode)
    // In 7-bit addressing mode (ADD10 = 0):
    // This bit is don’t care
    // In 10-bit addressing mode (ADD10 = 1):
    // This bit should be written with bit 0 of the slave address to be sent
    // Note: Changing these bits when the START bit is set is not allowed.
    enum class i2cdir { write = 0, read = 1 };
    void slave_addr(uint16_t addr, i2cdir read, bool is10bit = false) {
        uint32_t r = base->CR2;
        r &= ~I2C_CR2_SADD_Msk & ~I2C_CR2_RD_WRN;
        r |= (addr << 1) << I2C_CR2_SADD_Pos;
        if (read != i2cdir::write) r |= I2C_CR2_RD_WRN;
        base->CR2 = r;
    }
    uint16_t slave_addr(bool is10bit = false) const {
        uint16_t addr = (base->CR2 & ~I2C_CR2_SADD_Msk) >> I2C_CR2_SADD_Pos;
        return addr >> 1;
    }

    // I2C_OAR1
    //OA1EN: Own Address1 enable
    // 0: Own address 1 disabled. The received slave address OA1 is NACKed.
    // 1: Own address 1 enabled. The received slave address OA1 is ACKed.
    // OA1MODE Own Address110-bitmode
    // 0: Own address 1 is a 7-bit address.
    // 1: Own address 1 is a 10-bit address.
    // Note: This bit can be written only when OA1EN=0.
    // OA1[9:8]:Interfaceaddress
    // 7-bit addressing mode: don’t care
    // 10-bit addressing mode: bits 9:8 of address
    // Note: These bits can be written only when OA1EN=0.
    // OA1[7:1]:Interfaceaddress
    // Bits 7:1 of address
    // Note: These bits can be written only when OA1EN=0.
    // OA1[0]:Interfaceaddress
    // 7-bit addressing mode: don’t care
    // 10-bit addressing mode: bit 0 of address
    // Note: This bit can be written only when OA1EN=0.
    void own_address1(bool enable, uint16_t addr = 0, bool mode10bit = false) {
        uint32_t oar1 = base->OAR1;
        if (enable && (oar1 & I2C_OAR1_OA1EN))
            base->OAR1 &= ~I2C_OAR1_OA1EN;
        if (enable) {
            oar1 &= ~(I2C_OAR1_OA1MODE_Msk | I2C_OAR1_OA1_Msk);
            oar1 |= I2C_OAR1_OA1EN;
            assert(mode10bit || addr <= 0xffu);
            assert(mode10bit || (addr & 0x01) == 0);
            oar1 |= addr;
        }
        else
            oar1 &= ~I2C_OAR1_OA1EN;
        base->OAR1 = oar1;
    }
    uint16_t own_address1() {
        uint16_t addr = base->OAR1 & I2C_OAR1_OA1_Msk;
        return addr;
    }

    // I2C_OAR2
    // OA2EN:OwnAddress2 enable
    // 0: Own address 2 disabled. The received slave address OA2 is NACKed.
    // 1: Own address 2 enabled. The received slave address OA2 is ACKed.
    // Reserved,mustbekeptatresetvalue.
    // OA2MSK[2:0]:OwnAddress2 masks
    // 000: No mask
    // 001: OA2[1] is masked and don’t care. Only OA2[7:2] are compared.
    // 010: OA2[2:1] are masked and don’t care. Only OA2[7:3] are compared.
    // 011: OA2[3:1] are masked and don’t care. Only OA2[7:4] are compared.
    // 100: OA2[4:1] are masked and don’t care. Only OA2[7:5] are compared.
    // 101: OA2[5:1] are masked and don’t care. Only OA2[7:6] are compared.
    // 110: OA2[6:1] are masked and don’t care. Only OA2[7] is compared.
    // 111: OA2[7:1] are masked and don’t care. No comparison is done, and all (except reserved) 7-bit received addresses are acknowledged.
    // Note: These bits can be written only when OA2EN=0.
    // As soon as OA2MSK is not equal to 0, the reserved I2C addresses (0b0000xxx and 0b1111xxx) are not acknowledged even if the comparison matches.
    // OA2[7:1]:Interface address bits 7:1 of address
    // Note: These bits can be written only when OA2EN=0.
    // Reserved,must be kept at rese tvalue.
    void own_address2(bool enable, unsigned addr, unsigned masked_bits = 0) {
        uint32_t oar2 = base->OAR2;
        if (enable && (oar2 & I2C_OAR2_OA2EN))
            base->OAR2 &= ~I2C_OAR2_OA2EN;
        if (enable) {
            oar2 &= ~(I2C_OAR2_OA2MSK_Msk | I2C_OAR2_OA2_Msk);
            oar2 |= I2C_OAR2_OA2EN;
            oar2 |= addr;
        }
        else
            oar2 &= ~I2C_OAR2_OA2EN;

        base->OAR2 = oar2;
    }
    uint16_t own_address2() {
        uint16_t addr = base->OAR2 & I2C_OAR2_OA2_Msk;
        return addr;
    }

    // TIMINGR
    // PRESC[3:0]:Timing prescaler
    // This field is used to prescale I2CCLK in order to generate the clock period tPRESC used for data
    // setup and hold counters (refer to I2C timings on page 532) and for SCL high and low level counters (refer to I2C master initialization on page 547).
    // tPRESC = (PRESC+1) x tI2CCLK
    // SCLDEL[3:0]:Data setuptime
    // This field is used to generate a delay tSCLDEL between SDA edge and SCL rising edge. In master mode and in slave mode with NOSTRETCH = 0, the SCL line is stretched low during tSCLDEL.
    // tSCLDEL = (SCLDEL+1) x tPRESC
    // Note: tSCLDEL is used to generate tSU:DAT timing.
    // SDADEL[3:0]:Data holdtime
    // This field is used to generate the delay tSDADEL between SCL falling edge and SDA edge. In master mode and in slave mode with NOSTRETCH = 0, the SCL line is stretched low during tSDADEL.
    // tSDADEL= SDADEL x tPRESC
    // Note: SDADEL is used to generate tHD:DAT timing.
    // SCLH[7:0]:SCL high period(mastermode)
    // This field is used to generate the SCL high period in master mode. tSCLH = (SCLH+1) x tPRESC
    // Note: SCLH is also used to generate tSU:STO and tHD:STA timing.
    // SCLL[7:0]:SCL low period(mastermode)
    // This field is used to generate the SCL low period in master mode. tSCLL = (SCLL+1) x tPRESC
    // Note: SCLL is also used to generate tBUF and tSU:STA timings.    
    void timing(unsigned presc, unsigned scl_delay, unsigned sda_delay, unsigned scl_high, unsigned scl_low)
    {
        uint32_t timing = base->TIMINGR;
        timing &= ~(I2C_TIMINGR_PRESC_Msk | I2C_TIMINGR_SCLDEL_Msk | I2C_TIMINGR_SDADEL_Msk | I2C_TIMINGR_SCLL_Msk | I2C_TIMINGR_SCLH_Msk);
        timing |= presc << I2C_TIMINGR_PRESC_Pos;
        timing |= scl_delay << I2C_TIMINGR_SCLDEL_Pos;
        timing |= sda_delay << I2C_TIMINGR_SDADEL_Pos;
        timing |= scl_high << I2C_TIMINGR_SCLH_Pos;
        timing |= scl_low << I2C_TIMINGR_SCLL_Pos;
        base->TIMINGR = timing;
    }

    // I2C_TIMEOUTR
    // TEXTEN:Extended clock timeout enable
    // 0: Extended clock timeout detection is disabled
    // 1: Extended clock timeout detection is enabled. When a cumulative SCL stretch for more than tLOW:EXT is done by the I2C interface, a timeout error is detected (TIMEOUT=1).
    // TIMOUTEN:Clock timeout enable
    // 0: SCL timeout detection is disabled
    // 1: SCL timeout detection is enabled: when SCL is low for more than tTIMEOUT (TIDLE=0) or high for more than tIDLE (TIDLE=1), a timeout error is detected (TIMEOUT=1).
    void timeout(bool enable, bool ext_enable) {
        uint32_t r = base->TIMEOUTR;
        if (enable) r |= I2C_TIMEOUTR_TIMOUTEN; else r &= ~I2C_TIMEOUTR_TIMOUTEN;
        if (ext_enable) r |= I2C_TIMEOUTR_TEXTEN; else r &= ~I2C_TIMEOUTR_TEXTEN;
    }
    // TIMEOUTB[11:0]:Bustimeout B
    // This field is used to configure the cumulative clock extension timeout: In master mode, the master cumulative clock low extend time (tLOW:MEXT) is detected In slave mode, the slave cumulative clock low extend time (tLOW:SEXT) is detected tLOW:EXT= (TIMEOUTB+1) x 2048 x tI2CCLK
    // Note: These bits can be written only when TEXTEN=0.
    void timeout_b(unsigned v) {
        uint32_t r = base->TIMEOUTR;
        r &= I2C_TIMEOUTR_TIMEOUTB_Msk;
        r |= v << I2C_TIMEOUTR_TIMEOUTB_Pos;
        base->TIMEOUTR = r;
    }
    // TIMEOUTA[11:0]:BusTimeout A
    // This field is used to configure:
    // – The SCL low timeout condition tTIMEOUT when TIDLE=0
    // tTIMEOUT= (TIMEOUTA+1) x 2048 x tI2CCLK
    // – The bus idle condition (both SCL and SDA high) when TIDLE=1
    // tIDLE= (TIMEOUTA+1) x 4 x tI2CCLK
    void timeout_a(unsigned v) {
        uint32_t r = base->TIMEOUTR;
        r &= I2C_TIMEOUTR_TIMEOUTA_Msk;
        r |= v << I2C_TIMEOUTR_TIMEOUTA_Pos;
        base->TIMEOUTR = r;
    }
    // TIDLE:Idle clock timeout detection
    // 0: TIMEOUTA is used to detect SCL low timeout
    // 1: TIMEOUTA is used to detect both SCL and SDA high timeout (bus idle condition)
    // Note: This bit can be written only when TIMOUTEN=0.
    void timeout_idle(bool enable_both_scl_and_sdk_high) { IMPL_ENABLE_BIT(enable_both_scl_and_sdk_high, TIMEOUTR, I2C_TIMEOUTR_TIDLE); }

    // I2C_ISR
    uint16_t addr_code() { return (base->ISR & I2C_ISR_ADDCODE_Msk) >> I2C_ISR_ADDCODE_Pos; }
    enum class isr_flag : unsigned {
        dir = I2C_ISR_DIR,
        busy = I2C_ISR_BUSY,
        
        alert = I2C_ISR_ALERT,

        timeout = I2C_ISR_TIMEOUT,

        // PECERR:PEC Error in reception 
        // This flag is set by hardware when the received PEC does not match with the PEC register content. A NACK is automatically sent after the wrong PEC reception. It is cleared by software by setting the PECCF bit. Note: This bit is cleared by hardware when PE=0.        
        pecerr = I2C_ISR_PECERR,

        // OVR:Overrun/Underrun (slavemode)
        // This flag is set by hardware in slave mode with NOSTRETCH=1, when an overrun/underrun error occurs. It is cleared by software by setting the OVRCF bit. Note: This bit is cleared by hardware when PE=0.
        ovr = I2C_ISR_OVR,

        // ARLO:Arbitration lost
        // This flag is set by hardware in case of arbitration loss. It is cleared by software by setting the ARLOCF bit. Note: This bit is cleared by hardware when PE=0.
        arlo = I2C_ISR_ARLO,

        // BERR:Bus error
        // This flag is set by hardware when a misplaced Start or Stop condition is detected whereas the peripheral is involved in the transfer. The flag is not set during the address phase in slave mode. It is cleared by software by setting BERRCF bit. Note: This bit is cleared by hardware when PE=0.
        berr = I2C_ISR_BERR,

        any_error = I2C_ISR_BERR | I2C_ISR_ARLO | I2C_ISR_OVR | I2C_ISR_PECERR | I2C_ISR_TIMEOUT | I2C_ISR_ALERT,

        // TCR:Transfer Complete Reload
        // This flag is set by hardware when RELOAD=1 and NBYTES data have been transferred. It is cleared by software when NBYTES is written to a non-zero value.
        // Note: This bit is cleared by hardware when PE=0.
        tcr = I2C_ISR_TCR,

        // This flag is set by hardware when RELOAD=0, AUTOEND=0 and NBYTES data have been transferred. It is cleared by software when START bit or STOP bit is set.
        // Note: This bit is cleared by hardware when PE=0.
        tc = I2C_ISR_TC,

        // This flag is set by hardware when a Stop condition is detected on the bus and the peripheral is involved in this transfer:
        // – either as a master, provided that the STOP condition is generated by the peripheral.
        // – or as a slave, provided that the peripheral has been addressed previously during this transfer.
        // It is cleared by software by setting the STOPCF bit. Note: This bit is cleared by hardware when PE=0.        
        stopf = I2C_ISR_STOPF,
        nackf = I2C_ISR_NACKF,
        addr = I2C_ISR_ADDR,
        rxne = I2C_ISR_RXNE,

        // This bit is set by hardware when the I2C_TXDR register is empty and the data to be transmitted must be written in the I2C_TXDR register.
        // It is cleared when the next data to be sent is written in the I2C_TXDR register.
        // This bit can be written to ‘1’ by software when NOSTRETCH=1 only, in order to generate a TXIS event (interrupt if TXIE=1 or DMA request if TXDMAEN=1).
        // Note: This bit is cleared by hardware when PE=0.
        txis = I2C_ISR_TXIS,

        // TXE:Transmit data register empty(transmitters)
        // This bit is set by hardware when the I2C_TXDR register is empty. It is cleared when the next data to be sent is written in the I2C_TXDR register.
        // This bit can be written to ‘1’ by software in order to flush the transmit data register I2C_TXDR. Note: This bit is set by hardware when PE=0.        
        txe = I2C_ISR_TXE
    };
    bool isr_isset(isr_flag f) { return (base->ISR & static_cast<uint32_t>(f)) != 0; }
    void clear(isr_flag f) { base->ISR &= ~static_cast<uint32_t>(f); }
    void set(isr_flag f) { base->ISR |= static_cast<uint32_t>(f); }
    uint32_t isr() const { return base->ISR; }

    enum class icr_flag : uint32_t {
        // ALERTCF:Alert flag clear
        // Writing 1 to this bit clears the ALERT flag in the I2C_ISR register.
        // Note: If the SMBus feature is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 22.3: I2C implementation.
        alert = I2C_ICR_ALERTCF,

        // TIMOUTCF:Timeout detection flag clear
        // Writing 1 to this bit clears the TIMEOUT flag in the I2C_ISR register.
        // Note: If the SMBus feature is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 22.3: I2C implementation.
        timeout = I2C_ICR_TIMOUTCF,

        // PECCF:PEC Error flag clear
        // Writing 1 to this bit clears the PECERR flag in the I2C_ISR register.
        // Note: If the SMBus feature is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 22.3: I2C implementation.
        pec = I2C_ICR_PECCF,
        // OVRCF:Overrun/Underrun flag clear
        // Writing 1 to this bit clears the OVR flag in the I2C_ISR register.
        overrun_underrun = I2C_ICR_OVRCF,

        // ARLOCF:Arbitration Lost flag clear
        // Writing 1 to this bit clears the ARLO flag in the I2C_ISR register.
        arlo = I2C_ICR_ARLOCF,

        // BERRCF:Bus error flag clear
        // Writing 1 to this bit clears the BERRF flag in the I2C_ISR register.
        bus_error = I2C_ICR_BERRCF,

        // STOPCF:Stop detection flag clear
        // Writing 1 to this bit clears the STOPF flag in the I2C_ISR register.
        stop = I2C_ICR_STOPCF,

        // NACKCF:Not Acknowledge flag clear
        // Writing 1 to this bit clears the ACKF flag in I2C_ISR register.
        nack = I2C_ICR_NACKCF,

        // ADDRCF:Address matched flag clear
        // Writing 1 to this bit clears the ADDR flag in the I2C_ISR register. Writing 1 to this bit also clears
        // the START bit in the I2C_CR2 register.
        addr_matched = I2C_ICR_ADDRCF,
    };
    uint32_t clear(icr_flag f) { return base->ICR |= (uint32_t)f; }
    uint32_t icr() const { return base->ICR; }

    // PEC register I2C_PECR (r)
    // PEC[7:0]Packet error checking register
    // This field contains the internal PEC when PECEN=1. The PEC is cleared by hardware when PE=0.
    unsigned pec_value() const { return base->PECR & I2C_ISR_PECERR_Msk; }

    // Receive data register I2C_RXDR (r)
    // RXDATA[7:0]8-bit receive data
    // Data byte received from the I2C bus.
    uint8_t rx() { return (uint8_t)base->RXDR; }

    // Transmit data register I2C_TXDR (rw)
    // Reset value: 0x0000 0000 Access: No wait states
//    void tx(uint8_t a) { *((__IO uint8_t*)base->TXDR + 2) = a; }
//    void tx(uint8_t a) { base->TXDR = (base->TXDR & 0xffffff00) | a; }
    void tx(uint8_t a) { base->TXDR = a; }
    // used for dma
    void* tx_addr() const { return (void*)&base->TXDR; }
};

ENUM_FLAGS(i2c::isr_flag)
ENUM_FLAGS(i2c::icr_flag)
ENUM_FLAGS(i2c::intr)

}}

struct i2c_com
{
    enum sendfl : uint8_t {
        none = 0,
        // do not wait until transfer is finished (you should call finished/wait_finished some time later)
        nowait = 1,
        // do not send stop after transfer. will do RESTART condition with next send (is implied with seq).
        nostop = 2,
        // will continue with more data (using another send) after this data is sent; implies nostop.
        // does not generate stop or restart and thus does not send device address.
        seq = 4,
        // use DMA to send
        dma = 8
    };

    constexpr static stm32::p::dma::chan tx_dma_ch = stm32::p::dma::ch2;
    constexpr static stm32::p::dma::chan rx_dma_ch = stm32::p::dma::ch3;

private:
    uint8_t const* volatile m_data = nullptr;
    volatile uint16_t m_to_send = 0;
    volatile uint8_t m_flags = 0;

    static std::pair<bool, unsigned> calc_n(unsigned total) {
        uint32_t count = std::min(255u, (unsigned)total);
        return std::pair<bool, unsigned> { total > count, count };
    }

    void start_sending(bool cont);
    void start_sending_dma(bool cont);
    void issue_dma_transfer(void const* data, size_t size);

public:
    bool finished() const;
    void wait_finished();

    uint16_t remains() const { return m_to_send; }

    void send(uint8_t const* p, uint16_t count, sendfl fl = sendfl::none);
    void send(std::initializer_list<uint8_t> l, sendfl fl = sendfl::none) { send(std::begin(l), l.size(), fl); }

    void stop();

    void internal_irq();
    void internal_dma_interrupt();
};
ENUM_FLAGS(i2c_com::sendfl)

extern i2c_com g_i2c;

#undef IMPL_ENABLE_BIT

#endif
