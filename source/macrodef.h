#ifndef MACRODEF_H_FSwhWhdK
#define MACRODEF_H_FSwhWhdK

#define col_red "\x1b[1;31m"
#define col_green "\x1b[1;32m"
#define col_blue "\x1b[1;34m"
#define col_magenta "\x1b[1;35m"
#define col_x "\x1b[0m"

#ifdef __cplusplus

#define BEGIN_EXTERN_C_FOR_CPP extern "C" {
#define END_EXTERN_C_FOR_CPP }

#include <type_traits>

#define ENUM_FLAGS(T) \
    inline constexpr T operator|(T a, T b) { typedef std::underlying_type<T>::type U; return T((U)a | (U)b); } \
    inline constexpr T operator&(T a, T b) { typedef std::underlying_type<T>::type U; return T((U)a & (U)b); } \
    inline constexpr T operator^(T a, T b) { typedef std::underlying_type<T>::type U; return T((U)a ^ (U)b); }

#else

#define BEGIN_EXTERN_C_FOR_CPP
#define END_EXTERN_C_FOR_CPP

#endif

// Bitfield Value
#define BV(B, V) ( ((unsigned)(V) << B ## _Pos) & B ## _Msk)


#ifdef __arm__

BEGIN_EXTERN_C_FOR_CPP
void report_assert_failed(char const* expr, int line, char const* file);
END_EXTERN_C_FOR_CPP

#undef assert
#define assert(x) do { if (!(x)) report_assert_failed(#x, __LINE__, __FILE__); } while(0)
#endif

#endif