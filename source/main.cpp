#include <cstdio>
#include <cstdarg>
#include <string>
#include <cstring>
#include <cassert>

#include "parser.h"
#include "utils.h"
#include "config.h"

//#include "stm32f030xx.h"
#include "stm32f030x6.h"

#include "ssd1306.hpp"
#include "usart.hpp"
#include "rcc.hpp"
#include "gpio.hpp"

#include "printf.h"

#include "../tools/scroll_image_bw.c"
struct Communicator
{
	//std::string m_buffer;
	char m_in_buffer[256];
	size_t m_in_offset;
	size_t m_in_avail = 32;

public:
	Communicator() {}

	void push(char const* s, size_t size) {

	}

	bool expect(int params, char const* fmt, ...)
	{
		va_list ap;
		va_start(ap, fmt);
		char const* cur = parse_v(m_in_buffer, m_in_avail, "%d", ap);
//		int rc = vsscanf(m_buffer, fmt, ap);
		va_end(ap);

		return true;
	}
};

namespace stm32 {

	constexpr int ct_ctz(uint32_t x)
	{
		if (x == 0) return 32;
		int n = 0;
		if (!(x & 0x0000ffff)) { n += 16; x >>= 16; }
		if (!(x & 0x000000ff)) { n +=  8; x >>=  8; }
		if (!(x & 0x0000000f)) { n +=  6; x >>=  4; }
		if (!(x & 0x00000003)) { n +=  2; x >>=  2; }
		if (!(x & 0x00000001)) { n +=  1; } 
		return n;
	}

	constexpr int ct_clz(uint32_t x)
	{
		if (x == 0) return 32;
		int n=0;

		if (x & 0xffff0000) { n += 16; x <<= 16; }
		if (x & 0xff000000) { n +=  8; x <<=  8; }
		if (x & 0xf0000000) { n +=  4; x <<=  4; }
		if (x & 0xc0000000) { n +=  2; x <<=  2; }
		if (x & 0x80000000) { n +=  1; }

		return n;
	}

	template<uint32_t Mask>
	struct mv
	{
		uint32_t value;
		mv(uint32_t v) : value(v) {}
		
		enum { start = ct_ctz(Mask) };
		enum { mask = Mask };
		enum { size = start - ct_clz(Mask) };
	};

	struct masked_value
	{
		uint32_t mask = -1, value = 0;
	};

	template<uint32_t Mask>
	inline masked_value& operator <<(masked_value& m, mv<Mask> const& v) {
		m.mask |= mv<Mask>::mask;
		m.value = (v.value & mv<Mask>::mask) | (v.value << mv<Mask>::start);
		return m;
	}

	inline void operator >>(masked_value& m, volatile uint32_t& p) {
		p = p & m.mask | m.value;
	}


	template<uint32_t Mask>
	struct reg_access
	{
		enum { Start = ct_ctz(Mask) };
		//enum { Size = ct_clz(Mask) - Start };

		template<class T> static void set(T& r, unsigned value) { r = (r & ~Mask) | ((value << Start) & Mask); } 
		template<class T> static void get(T& r, unsigned value) { r = (r & ~Mask) | ((value << Start) & Mask); } 
		template<class T> static void or_(T& r, unsigned value) { r |= (value << Start) & Mask; } 
	};

	template<uint32_t Mask>
	struct bf
	{
		enum { Start = ct_ctz(Mask) };
		enum { Size = Start - ct_clz(Mask) };
		static constexpr uint32_t v(unsigned x) { return (x << Start) & Mask; }
	};

//		reg_access<RCC_CR_HSION>::set(RCC->CR, 3);
		
}


// void init_gpio1()
// {
// 	using namespace stm32::p;
// 	gpio a{GPIOA};
//     //void pin(unsigned pin, pin_mode mod, otype pushpull_opendrain, pin_speed spd, pupd pupd)
// 	a.pin(4, gpio::pin_mode::out, gpio::otype::push_pull, gpio::pin_speed::low, gpio::pupd::no_pull);

// 	// PA2, AF1:USART1_TX
// 	// PA9, AF1:USART1_TX
// 	// PA9, AF4:I2C_SCL
// 	a.pin(2, gpio::pin_mode::alternate, gpio::otype::push_pull, gpio::pin_speed::med, gpio::pupd::pull_up);
// 	a.alternate(2, 1);
// //	a.pin(9, gpio::pin_mode::out, gpio::otype::push_pull, gpio::pin_speed::med, gpio::pupd::pull_up);
// 	// PA3, AF1:USART2_RX
// 	// PA10, AF1:USART2_RX
// 	// PA10, AF4:I2C_SDA
// 	a.pin(3, gpio::pin_mode::alternate, gpio::otype::push_pull, gpio::pin_speed::med, gpio::pupd::pull_up);
// 	a.alternate(3, 1);

// }	

void init_gpio2()
{
	// 0:input 1:output, 2:alternate-function, 3:analog
	GPIOA->MODER = GPIOA->MODER
		& ~GPIO_MODER_MODER4_Msk & ~GPIO_MODER_MODER2_Msk & ~GPIO_MODER_MODER3_Msk
		| BV(GPIO_MODER_MODER4, 1)
		| BV(GPIO_MODER_MODER2, 2)| BV(GPIO_MODER_MODER3, 2)
		| BV(GPIO_MODER_MODER9, 2)| BV(GPIO_MODER_MODER10, 2)
		;

	// // x0: Low speed, 01:Medium speed, 11:High speed
	GPIOA->OSPEEDR = GPIOA->OSPEEDR
		& ~GPIO_OSPEEDR_OSPEEDR4_Msk & ~GPIO_OSPEEDR_OSPEEDR2_Msk & ~GPIO_OSPEEDR_OSPEEDR3_Msk
		| BV(GPIO_MODER_MODER2, 3) | BV(GPIO_MODER_MODER3, 3)
		| BV(GPIO_MODER_MODER9, 3) | BV(GPIO_MODER_MODER10, 3)
		;

	// // 0:push-pull 1:open-drain
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT_4
		& ~GPIO_OTYPER_OT_2 & ~GPIO_OTYPER_OT_3
		& ~GPIO_OTYPER_OT_9 & ~GPIO_OTYPER_OT_10
		;

	// // 00:No pull-up & pull-down, 01:Pull-up, 10:Pull-down, 11:Reserved
	GPIOA->PUPDR = GPIOA->PUPDR
		& ~GPIO_PUPDR_PUPDR4_Msk & ~GPIO_PUPDR_PUPDR2_Msk & ~GPIO_PUPDR_PUPDR3_Msk
		| BV(GPIO_MODER_MODER4, 0)
		| BV(GPIO_MODER_MODER2, 1) | BV(GPIO_MODER_MODER3, 1)
		| BV(GPIO_MODER_MODER9, 1) | BV(GPIO_MODER_MODER10, 1)
	;
	GPIOA->AFR[0] = GPIOA->AFR[0]
		& ~GPIO_AFRL_AFSEL2_Msk & ~GPIO_AFRL_AFSEL3_Msk
		| (1 << GPIO_AFRL_AFSEL2_Pos) | (1 <<GPIO_AFRL_AFSEL3_Pos) // USART
	;
	GPIOA->AFR[1] = GPIOA->AFR[1]
		& ~GPIO_AFRH_AFSEL9_Msk & ~GPIO_AFRH_AFSEL10_Msk
		| (4 << GPIO_AFRH_AFSEL9_Pos) | (4 <<GPIO_AFRH_AFSEL10_Pos) // I2C
	;
}

void init_peripherals()
{
	using namespace stm32::p;

	// Enable clocks for used peripherals
	rcc r(RCC);
	// r.enable(rcc::apb2::usart1|rcc::apb2::tim16);
	// r.enable(rcc::apb1::i2c1);
	// r.enable(rcc::ahb::porta);
	RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN | RCC_APB2ENR_TIM16EN;

	// output system clock on PA8
//	r.mco_clock(p::rcc::mco::system_clock, 0, true);
	
	using namespace stm32::p;
	usart u(USART1);
	//u.enable(false);
	u.enable(usart::rxtx::all);
	u.recv_timeout(true, 1024);
	u.stop_bits(usart::stop_bits::bit1);
	unsigned usartdiv = CLOCK_HZ / 57600;
	u.baud_rate(usartdiv, false);
	u.parity(false);
	u.word_length(usart::wl::s1_8bit);
//	u.clock(true);
	u.enable(true);


	init_gpio2();

	gpio a{GPIOA};
    //void pin(unsigned pin, pin_mode mod, otype pushpull_opendrain, pin_speed spd, pupd pupd)
	// a.pin(8, p::gpio::pin_mode::alternate, p::gpio::otype::push_pull, p::gpio::pin_speed::high, p::gpio::pupd::pull_up);
	// a.alternate(8, 0);


	/*
	// Configure Port A
	// pin0: ouput
	// pin2: alternate mode (usart)
	{
		// 0:input 1:output, 2:alternate-function, 3:analog
		masked_value moder;
		moder	<< mv<GPIO_MODER_MODER4>(1)
				<< mv<GPIO_MODER_MODER1>(1)
				<< mv<GPIO_MODER_MODER2>(2);
		moder >> GPIOA->MODER;

		// 0:push-pull 1:open-drain
		GPIOA->OTYPER |= bf<GPIO_OTYPER_OT_4>::v(0)
					  | bf<GPIO_OTYPER_OT_1>::v(0);
		
		// x0: Low speed, 01:Medium speed, 11:High speed
		GPIOA->OSPEEDR |= bf<GPIO_OSPEEDR_OSPEEDR4>::v(0)
						| bf<GPIO_OSPEEDR_OSPEEDR1>::v(0)
					    | bf<GPIO_OSPEEDR_OSPEEDR2>::v(3);

		// 00:No pull-up & pull-down, 01:Pull-up, 10:Pull-down, 11:Reserved
		GPIOA->PUPDR |= bf<GPIO_PUPDR_PUPDR4>::v(0)
					  | bf<GPIO_PUPDR_PUPDR1>::v(0)
					  | bf<GPIO_PUPDR_PUPDR2>::v(1);
	}*/
	
	//init_gpio2();
}

// #include "ring_buffer.h"
// ring_buffer<uint8_t, 32> i2c_wrbuf;
// enum class i2c_com { ok, error };
// i2c_com i2cst = i2c_com::ok;

// void i2c_isr()
// {
// 	using namespace stm32::p;

// 	i2c iic{I2C1};
// 	if (iic.isr_isset(i2c::isr_flag::txe)) {
// 		if (!i2c_wrbuf.empty()) {
// 			iic.tx(i2c_wrbuf.front_and_pop());
// 			return;
// 		}
// 	} else if (iic.isr_isset(i2c::isr_flag::any_error))
// 	{
// 	}
// }

// void i2c_send(char const* buf, size_t count)
// {
// 	while (count > 0) {
// 		__disable_irq();
// 		if (!i2c_wrbuf.full())
// 			i2c_wrbuf.push_back(*(--count, buf++));
// 		else

// 		__enable_irq();
// 	}
// }

int main()
{
	//Communicator comm;
	init_peripherals();
	init_utils(utils_ms_timer);

	delay_ms(50);
	
	signal_blink(3, 100);
	delay_ms(300);

	using namespace stm32::p;
	dbg_printf("> start\r\n");

	unsigned addr = 0x3c;
	delay_ms(1000);

	i2c iic{I2C1};

	I2C1->CR1 &= ~I2C_CR1_PE;
//	iic.timing(1, 0x4, 0x2, 0xc3, 0xc7); // 10kHz @fi2cclk=8Mhz (100us clock)
//	iic.timing(1, 0x4, 0x2, 0xf, 0x13); // 100kHz @fi2cclk=8Mhz (10us clock)
	iic.timing(1, 0x3, 0x1, 0x3, 0x3); // 400kHz @fi2cclk=8Mhz  (2.5us clock) -- 7us
	//iic.digital_noise_filter(8);
	iic.enable(true);

	iic.slave_addr(addr, false);
	iic.comm_dir(i2c::i2cdir::write);

	// <START> addr, Co|D/C|000000, ..., <STOP>
	// addr - slave address 
	// Co - 0:data 1:command
	// D/C - next byte data or command. 0:command 1:data which will be stored in GDDRAM. column addr point will be increased automatically

	// g_i2c.send( {0x00, ssd1306::cmd::col_addr, 0, ssd1306::width-1} );
	// g_i2c.send( {0x00, ssd1306::cmd::page_addr, 0, height/8-1} );
	// dbg_printf("  PAGE_ADDR DONE\r\n");

	// auto mywait = [&iic]() { while(!g_i2c.finished()) dbg_printf("%08x\r\n", iic.isr()); };
	// g_i2c.send( {0x40, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}, i2c_com::sendfl::none);
	// g_i2c.wait_finished();
    // g_i2c.send( {0x40}, i2c_com::sendfl::seq|i2c_com::sendfl::wait);
    // g_i2c.send( {0x0f, 0x0f }, i2c_com::sendfl::wait|i2c_com::sendfl::nostop);
	// g_i2c.send( {0x40, 0xff}, i2c_com::sendfl::wait);
	// dbg_printf("  0xf 0xf DONE\r\n");
	// dbg_printf("%08x\r\n", iic.isr());

	// assert(!"HALT");

	ssd1306 lcd;
	lcd.init();
	dbg_printf("lcd init DONE\r\n");
	// lcd.set_point(10, 10);

	unsigned rate = 10;
	unsigned s = now_ms();
	int y = 0, pre_y = 128;
	int frame = 0;
	while (true) {
		unsigned n = now_ms();
		if (n - s >= rate) {
			s += rate;
#if 0
			lcd.clear();
			for (int x=0; x<lcd.width; ++x)
				lcd.set_point(x, y);

			lcd.send_buffer();
			y = (y+1) % ssd1306::height;
#else
			// screen is rotated 180
			unsigned pre, ys, xfer;
			if (y < pre_y) {
				ys = 0;
				pre = pre_y - y;
				xfer = lcd.width - pre;
			} else {
				ys = y - pre_y;
				pre = 0;
				xfer = std::min<unsigned>(lcd.width, static_image.h - ys);
			}

			assert(lcd.height == static_image.w);

			for (int i=0; i<8; ++i) {
				unsigned o = lcd.width * (8-1-i);
				unsigned src_ofs = static_image.h * i + ys;
				// for (int q=0; q<lcd.width; ++q)
				// 	lcd.m_data[o+q] = static_image.p[src_ofs + q];
				memset(lcd.m_data + o, 0, pre);
				memcpy(lcd.m_data + o + pre, static_image.p + src_ofs, xfer);
			}

			y = (y+1) % (static_image.h + pre_y);
			lcd.send_buffer();
#endif
		}

			dbg_printf("frame %d, %d ms       \r", frame++, now_ms() - n);

//		dbg_printf("SEND DONE %d\r\n", i++);
		// signal_blink(10, 100);
	}

	//send_cmd( {0x80, 0x8d} );

 	// iic.num_bytes(3); iic.start();
 	// I2C1->TXDR = 0x80; iic.wait_tx_empty();
 	// I2C1->TXDR = 0x8d; iic.wait_tx_empty();
	// I2C1->TXDR = 0x14; iic.wait_tx_empty();
	
	// iic.num_bytes(2); iic.start();
	// I2C1->TXDR = 0x80; iic.wait_tx_empty();
	// I2C1->TXDR = 0xaf; iic.wait_tx_empty();

	// iic.num_bytes(2);	iic.start();
	// I2C1->TXDR = 0x80; iic.wait_tx_empty();
	// I2C1->TXDR = 0xa5; iic.wait_tx_empty();
	// iic.stop();

	signal_blink(50, 200);

	delay_ms(10);

	while (!(I2C1->ISR & I2C_ISR_TXE))
		;

	I2C1->CR2 |= I2C_CR2_STOP;

	signal_blink(5, 500);

	iic.enable(true);
	iic.autoend(true);
	iic.timing(1, 0x4, 0x2, 0xc3, 0xc7);
//	iic.start();
//	iic.stop();
	iic.num_bytes(1);//, false);
//	iic.address(addr, false);
//	iic.base->CR2 = I2C_CR2_AUTOEND | (1 << 16) | (addr << 1);
	iic.timeout_a(0x61);
	for (int i=0; ; ++i) {
		iic.base->ISR |= I2C_ISR_TXE;

		dbg_printf("hello world: %d\r\n", i);
		signal_blink(2, 70);
				
		delay_ms(10);
	}
}
