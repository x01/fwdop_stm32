#ifndef NVIC_INCLUDED_FSwhWhdK_H_
#define NVIC_INCLUDED_FSwhWhdK_H_
#pragma once

#include <stm32f030x6.h>

namespace stm32::p {

struct nvic
{
    static void enable(bool enable, IRQn_Type chan, unsigned prio = 0)
    {
        assert(prio < (1 << __NVIC_PRIO_BITS));
        if (enable)
        {
            unsigned i = chan >> 2;
            unsigned j = (chan & 3) << 3;

            uint32_t ip = NVIC->IP[i];
            ip &= ~(0xffu << j);
            ip |= ((prio << 6) & 0xff) << j;
            NVIC->IP[i] = ip;

            // enable
            NVIC->ISER[0] = 0x01u << (chan & 0x1Fu);
        }
        else
        {
            // disable
            NVIC->ICER[0] = 0x01u << (chan & 0x1Fu);
        }
    }
};

}

#endif
