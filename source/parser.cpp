#include <cstdio>
#include <cstdarg>
#include <string>

#include "parser.h"

char const* parse_digit(char const* str, size_t avail, int& result) {

	if (avail == 0) return str;

	int minus = 1;
	if (*str == '-') { ++str; minus=-1; avail--; }

	int value = 0;
	while (avail --> 0) {
		
		int c = *str;
		if (c < '0' || c > '9') break;

		value *= 10;
		value += c - '0';
		++str;
	}

	value *= minus;
	result = value;

	return str;
}

char const* parse(char const* str, size_t avail, char const* fmt, ...) {
	va_list vl;
	va_start(vl, fmt);
	char const* result = parse_v(str, avail, fmt, vl);
	va_end(vl);
	return result;
}

char const* parse_v(char const* str, size_t avail, char const* fmt, va_list va) {
	char const* end = str + avail;
	bool digit = false, string = false, quoted = false;

	va_list vl;
	va_copy(vl, va);

	while(*fmt) {
		if (*fmt == '%') {

			quoted = false;
			if (!*++fmt) break;

			if (*fmt == 'q') { quoted = true; if (!*++fmt) break; }

			switch(*fmt) {
				case 'd': digit = true; break;
				case 's': string = true; break;
				default: ;
			}
			++fmt;
		} else if (*fmt == ' ') {
			while(isspace(*str))
				++str;
			++fmt;

			continue;
		}

		if (digit || string)
			while(isspace(*str)) ++str;

		if (digit) {
			int int_val;
			char const* dig_end = parse_digit(str, end - str, int_val);
			*va_arg(vl, int*) = int_val;
			str = dig_end;

			digit = false;
		} else if (string) {
			int q = 0;
			if (quoted) {
				if (*str == '\'') q |= 1; else if (*str == '\"') q |= 2;
			}
			if (q) ++str;
			char const* b = str, *e = str;
			while (1) {
				int c = *str;

				if (!c) { e = str; break; }

				if (quoted) {
					if (q & 1) {
						if (c == '\'') { e = str++; break; }
					} else if (q & 2) {
						if (c == '"') { e = str++; break; }
					}
				} else {
					if (isspace(c)) { e = str++; break; }
				}
				++str;
			}

			str_len* p_sl = va_arg(vl, str_len*);
			p_sl->str = b;
			p_sl->len = e - b;

			string = false;

		} else {
			if (*str++ != *fmt++)
				break;
		}
	}

	va_end(vl);

	return str;
}

