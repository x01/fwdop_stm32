#ifndef FWDOP_PARSER_H_
#define FWDOP_PARSER_H_

#include <stddef.h>
#include <stdarg.h>

struct str_len { char const* str; int len; };

char const* parse_digit(char const* str, size_t avail, int& result);
char const* parse_v(char const* str, size_t avail, char const* fmt, va_list va);
char const* parse(char const* str, size_t avail, char const* fmt, ...);

#endif

