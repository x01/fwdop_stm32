#include <cstdint>
#include <cstddef>
#include <cstdarg>
#include <cstring>
#include <cassert>
#include <algorithm>
#include "macrodef.h"
#include "printf.h"

#define SEND_TO_USART 0
#define SEND_TO_ITM 1

#define DIGITS_BOUND(t) (241 * sizeof(t) / 100 + 1)

enum Flags : unsigned { none, caps=1, fillzero=2, minus=4 };

char* to_str(char* buf, size_t bufsize, unsigned v, int width, Flags fmt) {
    char* p = buf + bufsize;
    do {
        int d = v % 10;
        v /= 10;
        *--p = '0' + d;
    } while(v);

    if ((fmt & Flags::minus) && !(fmt&Flags::fillzero)) *--p = '-';

    while (p > buf + bufsize - width)
        *--p = (fmt&Flags::fillzero) ? '0' : ' ';

    // when fillzero is set, width should be decremented by the calling function
    if ((fmt & Flags::minus) && (fmt&Flags::fillzero)) *--p = '-';

    assert (p >= buf);

    return p;
}

char* to_str_x(char* buf, size_t bufsize, unsigned v, int width, Flags fmt)
{
    char* p = buf + bufsize;
    do {
        unsigned d = (v & 0xf);
        unsigned c = (d >= 0x0au ? ((fmt&Flags::caps ? 'A' : 'a' )-0x0au) : '0') + d;
        *--p = c;
        v >>= 4;
    } while(v);

    while (p > buf + bufsize - width)
        *--p = (fmt&Flags::fillzero) ? '0' : ' ';

    assert (p >= buf);

   return p;
}

char* to_str_i(char* buf, size_t bufsize, int v, int width, Flags fmt) {
    unsigned uv = (v < 0) ? -v : v;
    if (v < 0) { fmt = Flags(fmt | Flags::minus); if (fmt & Flags::fillzero) width--; }
    char* p = to_str(buf, bufsize, uv, width, fmt);
    
    assert (p >= buf);
    
    return p;
}

// size_t dbg_putsi(int v, int width, Flags fmt) {
// 	char buf[DIGITS_BOUND(int) + 1];

//     unsigned uv = (v < 0) ? -v : v;
//     char* p = to_str(buf, sizeof buf, uv, width, fmt);
//     if (uv != (unsigned)v)
//         *--p = '-';

//     while(*p)
//         dbg_putchar(*p++);
// }

// void dbg_putsu(unsigned v, int width, Flags fmt) {
// 	char buf[DIGITS_BOUND(v) + 1];
//     char* p = to_str(buf, sizeof buf, v, width, fmt);
//     while(*p)
//         dbg_putchar(*p++);
// }

// void dbg_putsx(unsigned v, int width, Flags fmt)
// {
// 	char buf[DIGITS_BOUND(v) + 1];
//     char* p = to_str_x(buf, sizeof buf, v, width, fmt);
//     while(*p)
//         dbg_putchar(*p++);
// }

#ifdef __arm__
#include "usart.hpp"

struct PrintContext_UART {
    void puts(char const* s, size_t size) {
        stm32::p::usart u{USART1};
        while (size --> 0) {
            while (!u.status(stm32::p::usart::isr::tx_empty))
                ;
            u.transmit(*s++);
        }
        while (!u.status(stm32::p::usart::isr::tx_complete))
            ;
    }
};
#endif

struct PrintContext_MEM {
    char* m_dest, *m_end;
    PrintContext_MEM(char* p, size_t size) : m_dest(p), m_end(m_dest + size) {}
    void puts(char const* s, size_t size) { for( ; size --> 0 && (*m_dest++ = *s++) && m_dest < m_end; ) ; }
};

static char parse_digit_or_0(char c, char const*& s, size_t& dig) {
    while (c >= '0' && c <= '9') {
        dig += c - '0';
        c = *s++;
        if (!c || !(c >= '0' && c <= '9') ) break;
        dig *= 10;
    }
    return c;
}


template<class PrintContext>
struct PrintfAlgo
{
    PrintContext m_ctx;
    PrintfAlgo(PrintContext ctx) : m_ctx(ctx) {}

    size_t printf(char const* s, va_list vl)
    {
        size_t count = 0;
        char const* span = s;
        while(1) {
            char c = *s++;
            if (!c) {
                m_ctx.puts(span, s-span);
                break;
            }
            if (c == '%') {
                size_t cnt = s-span;
                if (cnt > 1)
                    m_ctx.puts(span, cnt-1);

                char c1 = *s++;
                if (!c1) break;

                Flags fmt { Flags::none };
                size_t width = 0;
#if PRINTF_SUPPORTS_WIDTH
                if (c1 == '0') {
                    fmt = Flags(fmt | Flags::fillzero);
                    if (!(c1 = *s++)) break;
                }

                c1 = parse_digit_or_0(c1, s, width);
#endif
                size_t prec = 0;
#if PRINTF_SUPPORTS_PRECISION
                 if (c1 == '.') {
                     if (!(c1 = *s++)) break;
                     if (c1 == '*') {
                         prec = (int)va_arg(vl, int);
                         c1 = *s++;
                     } else
                         c1 = parse_digit_or_0(c1, s, prec);
                 }
#endif
                span = s;

                if (c1 == 's')
                {
                    char const* str = (char const*)va_arg(vl, char*);
                    size_t slen = strlen(str);
                    size_t toput = prec == 0 ? slen : std::min(slen, prec);
                    if (toput < width) {
                        size_t pad = width-toput;
                        for (int i=0; i<pad; ++i)
                            m_ctx.puts(" ", 1);
                        count+=pad;
                    }
                    m_ctx.puts(str, toput);
                    count += toput;
                }
                else
                {
                    char buf[DIGITS_BOUND(unsigned)+2];
                    char* bufend = buf + sizeof buf - 1;
                    char* p = bufend;
                    *p = 0;
                    switch(c1) {
                        case '%':
                        ++count;
                        m_ctx.puts(&c1, 1);
                        break;

                        case 'x':
                        {
                            unsigned d = va_arg(vl, unsigned);
                            p = to_str_x(buf, sizeof buf - 1, d, width, fmt);
                            break;
                        }

                        case 'u':
                        {
                            unsigned d = va_arg(vl, unsigned);
                            p = to_str(buf, sizeof buf - 1, d, width, fmt);
                            break;
                        }

                        case 'd':
                        {
                            int d = va_arg(vl, int);
                            p = to_str_i(buf, sizeof buf - 1, d, width, fmt);
                            break;
                        }
                    }
                    count += bufend - p;
                    m_ctx.puts(p, bufend - p);
                }
            }
            else
            {
                count++;
                // m_ctx.putc(c);
            }
        }

        va_end(vl);

        return count;
    }

};

#ifdef __arm__
extern "C" int dbg_printf(char const* s, ...)
{
    typedef PrintfAlgo<PrintContext_UART> algo_t;
    va_list vl;
    va_start(vl, s);
    algo_t algo{ PrintContext_UART{} };
//    __disable_irq();
    size_t sz = algo.printf(s, vl);
//    __enable_irq();
    va_end(vl);
    return sz;
}
#endif

extern "C" int snprintf(char* dest, size_t bufsize, char const* s, ...)
{
    typedef PrintfAlgo<PrintContext_MEM> algo_t;
    va_list vl;
    va_start(vl, s);
    algo_t algo{ PrintContext_MEM{dest, bufsize} };
    size_t sz = algo.printf(s, vl);
    va_end(vl);
    return sz;
}
