#ifndef PRINTF_INCLUDED_FSwhWhdK_H_
#define PRINTF_INCLUDED_FSwhWhdK_H_

#include <stddef.h>
#include "macrodef.h"

BEGIN_EXTERN_C_FOR_CPP

#define PRINTF_SUPPORTS_PRECISION 1
#define PRINTF_SUPPORTS_WIDTH 1

int dbg_printf(char const* s, ...);
int snprintf(char* dest, size_t bufsize, char const* s, ...);

END_EXTERN_C_FOR_CPP

#endif
