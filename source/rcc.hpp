#ifndef RCC_INCLUDED_FSwhWhdK_H_
#define RCC_INCLUDED_FSwhWhdK_H_

#include <stm32f030x6.h>

#include <cstddef>
#include "utils.h"

namespace stm32 { namespace p {

    namespace reg
    {
        struct cr {
            union {
                uint32_t reg;
                struct {

                };
            };
        };
    }

// Reset and clock control (RCC)
struct rcc
{
    RCC_TypeDef* base;

    rcc() {}
    rcc(RCC_TypeDef* base) : base(base) {}

#define IMPL_ENABLE_BIT(COND, REG, FLAG) do { if (COND) base->REG |= FLAG; else base->CR1 &= ~FLAG; } while (0)

    // Clock control register (RCC_CR)
    // Reset value: 0x0000 XX83 where X is undefined.
    enum class clock_ready : uint32_t {
        // PLLRDY:PLL clock ready flag
        // Set by hardware to indicate that the PLL is locked.
        // 0: PLL unlocked 1: PLL locked
        pll = RCC_CR_PLLRDY,
        // HSERDY:HSE clock ready flag (high speed external)
        // Set by hardware to indicate that the HSE oscillator is stable. This bit needs 6 cycles of the HSE oscillator clock to fall down after HSEON reset.
        // 0: HSE oscillator not ready 1: HSE oscillator ready
        hse = RCC_CR_HSERDY,
        // HSIRDY:HSI clock ready flag (high speed internal)
        // Set by hardware to indicate that HSI oscillator is stable. After the HSION bit is cleared, HSIRDY goes low after 6 HSI oscillator clock cycles.
        // 0: HSI oscillator not ready 1: HSI oscillator ready
        hsi = RCC_CR_HSIRDY
    };
    bool is_clock_ready(clock_ready fl) { return base->CR & (std::underlying_type_t<clock_ready>)fl; }

    enum class clock_on : uint32_t {
        none = 0,

        // PLLON:PLL enable
        // Set and cleared by software to enable PLL.
        // Cleared by hardware when entering Stop or Standby mode. This bit can not be reset if the PLL clock is used as system clock or is selected to become the system clock.
        // 0: PLL OFF 1: PLL ON
        pll = RCC_CR_PLLON,

        // CSSON:Clock security system enable
        // Set and cleared by software to enable the clock security system. When CSSON is set, the clock detector is enabled by hardware when the HSE oscillator is ready, and disabled by hardware if a HSE clock failure is detected.
        // 0: Clock security system disabled (clock detector OFF).
        // 1: Clock security system enabled (clock detector ON if the HSE is ready, OFF if not).
        css = RCC_CR_CSSON,

        // HSEON:HSE clock enable Set and cleared by software.
        // Cleared by hardware to stop the HSE oscillator when entering Stop or Standby mode. This bit cannot be reset if the HSE oscillator is used directly or indirectly as the system clock.
        // 0: HSE oscillator OFF 1: HSE oscillator ON
        hse = RCC_CR_HSEON,

        // HSION:HSI clock enable Set and cleared by software.
        // Set by hardware to force the HSI oscillator ON when leaving Stop or Standby mode or in case of failure of the HSE crystal oscillator used directly or indirectly as system clock. This bit cannot be reset if the HSI is used directly or indirectly as system clock or is selected to become the system clock.
        // 0: HSI oscillator OFF
        // 1: HSI oscillator ON  
        hsi = RCC_CR_HSION,

        // HSEBYP:HSE crystal oscillator bypass
        // Set and cleared by software to bypass the oscillator with an external clock. The external clock must be enabled with the HSEON bit set, to be used by the device. The HSEBYP bit can be written only if the HSE oscillator is disabled.
        // 0: HSE crystal oscillator not bypassed
        // 1: HSE crystal oscillator bypassed with external clock
        hse_bypass = RCC_CR_HSEBYP

    };
    bool is_on(clock_on fl) const { return base->CR & (std::underlying_type_t<clock_on>)fl; }
    void enable(clock_on en, clock_on dis = clock_on::none) { base->CR = base->CR & (std::underlying_type_t<clock_on>)dis | (std::underlying_type_t<clock_on>)en; }

    // HSICAL[7:0]: HSI clock calibration
    // These bits are initialized automatically at startup. They are adjusted by SW through the HSITRIM setting.
    uint8_t hsi_calibration() const { return (base->CR & RCC_CR_HSICAL_Msk) >> RCC_CR_HSICAL_Pos; }

    // HSITRIM[4:0]:HSI clock trimming
    // These bits provide an additional user-programmable trimming value that is added to the HSICAL[7:0] bits. It can be programmed to adjust to variations in voltage and temperature that influence the frequency of the HSI.
    // The default value is 16, which, when added to the HSICAL value, should trim the HSI to 8 MHz ± 1%. The trimming step is around 40 kHz between two consecutive HSICAL steps.
    // Note: Increased value in the register results to higher clock frequency.
    uint8_t hsi_trim() const { return (base->CR & RCC_CR_HSITRIM_Msk) >> RCC_CR_HSICAL_Pos; }
    void hsi_trim(uint8_t clock_trimming) { base->CR = base->CR & RCC_CR_HSITRIM_Msk | (clock_trimming << RCC_CR_HSICAL_Pos); }

    // Clock configuration register (RCC_CFGR)
    // Reset value: 0x0000 0000
    // Access: 0 ≤ wait state ≤ 2, word, half-word and byte access 1 or 2 wait states inserted only if the access occurs during clock source switch.
    enum class mco {
        no_output = RCC_CFGR_MCO_NOCLOCK, internal_rc_14mhz = RCC_CFGR_MCO_HSI14,
        internal_low_speed = RCC_CFGR_MCO_LSI, external_low_speed = RCC_CFGR_MCO_LSE,
        system_clock = RCC_CFGR_MCO_SYSCLK,
        inernal_rc_8mhz = RCC_CFGR_MCO_HSI, external_4_32mhz = RCC_CFGR_MCO_HSE,
        pll = RCC_CFGR_MCO_PLL };
    // PLLNODIV:PLL clock not divided for MCO (not available on STM32F030x8devices)
    // This bit is set and cleared by software. It switches off divider by 2 for PLL connection to MCO.
    // 0: PLL is divided by 2 for MCO 1: PLL is not divided for MCO
    // MCOPRE[2:0]:Microcontroller ClockOutput Prescaler(notavailableonSTM32F030x8 devices)
    // These bits are set and cleared by software to select the MCO prescaler division factor. To avoid glitches, it is highly recommended to change this prescaler only when the MCO output is disabled.
    // 000: MCO is divided by 1
    // 001: MCO is divided by 2
    // 010: MCO is divided by 4
    // .....
    // 111: MCO is divided by 128
    // MCO[3:0]:Microcontroller clock output Set and cleared by software.
    // 0000: MCO output disabled, no clock on MCO
    // 0001: Internal RC 14 MHz (HSI14) oscillator clock selected
    // 0010: Internal low speed (LSI) oscillator clock selected
    // 0011: External low speed (LSE) oscillator clock selected
    // 0100: System clock selected
    // 0101: Internal RC 8 MHz (HSI) oscillator clock selected
    // 0110: External 4-32 MHz (HSE) oscillator clock selected
    // 0111: PLL clock selected (divided by 1 or 2, depending on PLLNODIV) 1xxx: Reserved, must be kept at reset value.
    // Note: This clock output may have some truncated cycles at startup or during MCO clock source switching.  
    void mco_clock(mco clock, unsigned mco_prescaler_shift_by, bool pll_no_div2_for_mco) {
        uint32_t r = base->CFGR & ~(
            RCC_CFGR_PLLNODIV_Msk |
            RCC_CFGR_MCOPRE_Msk |
            RCC_CFGR_MCO_Msk);
        r |= (uint32_t)pll_no_div2_for_mco << RCC_CFGR_PLLNODIV_Pos;
        r |= (uint32_t)mco_prescaler_shift_by << RCC_CFGR_MCOPRE_Pos;
        r |= (uint32_t)clock;
        base->CFGR = r;
    }

    enum class pllsrc {
        hsi_div2 = RCC_CFGR_PLLSRC_HSI_DIV2,
        hse_prediv = RCC_CFGR_PLLSRC_HSE_PREDIV
    };
    // PLLSRC:PLL entry clock source
    // Set and cleared by software to select PLL clock source. This bit can be written only when PLL is disabled.
    // 0: HSI/2 selected as PLL input clock
    // 1: HSE/PREDIV selected as PLL input clock (refer to Section 7.4.12: Clock configuration register 2 (RCC_CFGR2) on page 122)
    // PLLMUL[3:0]:PLL multiplication factor
    // These bits are written by software to define the PLL multiplication factor. These bits can be written only when PLL is disabled.
    // Caution: The PLL output frequency must not exceed 48 MHz.
    // 0000: PLL input clock x 2 0001: PLL input clock x 3 0010: PLL input clock x 4 0011: PLL input clock x 5 0100: PLL input clock x 6 0101: PLL input clock x 7 0110: PLL input clock x 8 0111: PLL input clock x 9 1000: PLL input clock x 10 1001: PLL input clock x 11 1010: PLL input clock x 12 1011: PLL input clock x 13 1100: PLL input clock x 14 1101: PLL input clock x 15 1110: PLL input clock x 16 1111: PLL input clock x 16
    // PLLXTPRE:HSE divider for PLL input clock
    // This bit is the same bit as bit PREDIV[0] from RCC_CFGR2. Refer to RCC_CFGR2 PREDIV bits description for its meaning.
    void pll(pllsrc pll_source, unsigned pllmul, bool hse_div_for_pll) {
        uint32_t r = base->CFGR & ~(RCC_CFGR_PLLSRC_Msk | RCC_CFGR_PLLMUL_Msk | RCC_CFGR_PLLXTPRE_Msk);
        r |= (uint32_t)pll_source;
        r |= (uint32_t)pllmul << RCC_CFGR_PLLMUL_Pos;
        r |= (uint32_t)hse_div_for_pll << RCC_CFGR_PLLXTPRE_Pos;
        base->CFGR = r;
    }

    enum class ppre : uint32_t { div1 = RCC_CFGR_PPRE_DIV1, div2 = RCC_CFGR_PPRE_DIV2, div4 = RCC_CFGR_PPRE_DIV4, div8 = RCC_CFGR_PPRE_DIV8, div16 = RCC_CFGR_PPRE_DIV2 };
    // PPRE[2:0]:PCLK prescaler
    // Set and cleared by software to control the division factor of the APB clock (PCLK).
    // 0xx: HCLK not divided 100: HCLK divided by 2 101: HCLK divided by 4 110: HCLK divided by 8 111: HCLK divided by 16
    void pclk_prescaler(ppre prescale) { base->CFGR = base->CFGR & RCC_CFGR_PPRE_Msk | (uint32_t)prescale; }

    enum class hpre : uint32_t {
        div1 = RCC_CFGR_HPRE_DIV1,
        div2 = RCC_CFGR_HPRE_DIV2,
        div4 = RCC_CFGR_HPRE_DIV4,
        div8 = RCC_CFGR_HPRE_DIV8,
        div16 = RCC_CFGR_HPRE_DIV16,
        div64 = RCC_CFGR_HPRE_DIV64,
        div128 = RCC_CFGR_HPRE_DIV128,
        div256 = RCC_CFGR_HPRE_DIV256,
        div512 = RCC_CFGR_HPRE_DIV512,
    };
    // HPRE[3:0]:HCLK prescaler
    // Set and cleared by software to control the division factor of the AHB clock.
    // 0xxx: SYSCLK not divided 1000: SYSCLK divided by 2 1001: SYSCLK divided by 4 1010: SYSCLK divided by 8 1011: SYSCLK divided by 16 1100: SYSCLK divided by 64 1101: SYSCLK divided by 128 1110: SYSCLK divided by 256 1111: SYSCLK divided by 512
    void sysclk_prescaler(hpre prescale) { base->CFGR = base->CFGR & RCC_CFGR_HPRE_Msk | (uint32_t)prescale; }

    enum class clock_source : uint32_t { hsi = RCC_CFGR_SW_HSI, hse = RCC_CFGR_SW_HSE, pll = RCC_CFGR_SW_PLL };
    // SWS[1:0]:System clock switch status
    // Set and cleared by hardware to indicate which clock source is used as system clock.
    // 00: HSI oscillator used as system clock 01: HSE oscillator used as system clock 10: PLL used as system clock
    // 11: Reserved, must be kept at reset value.
    // SW[1:0]:System clock switch
    // Set and cleared by software to select SYSCLK source.
    // Cleared by hardware to force HSI selection when leaving Stop and Standby mode or in case of failure of the HSE oscillator used directly or indirectly as system clock (if the Clock Security System is enabled).
    // 00: HSI selected as system clock
    // 01: HSE selected as system clock
    // 10: PLL selected as system clock
    // 11: Reserved, must be kept at reset value.
    void switch_clock(clock_source source) { base->CFGR = base->CFGR & RCC_CFGR_SW_Msk | std::underlying_type_t<clock_source>(source); }
    clock_source switch_clock() { return (clock_source)(base->CFGR & RCC_CFGR_SW_Msk); }
    clock_source clock_status() { return (clock_source)((base->CFGR & RCC_CFGR_SWS_Msk) >> RCC_CFGR_SWS_Pos); }

    // Clock interrupt register (RCC_CIR)
    // Address offset: 0x08
    // Reset value: 0x0000 0000
    enum class intr_clear : uint32_t {
        // CSSC:Clock security system interrupt clear This bit is set by software to clear the CSSF flag.
        // 0: No effect 1: Clear CSSF flag
        clock_security = RCC_CIR_CSSC,

        // HSI14RDYC:HSI14 ready interrupt clear
        // This bit is set by software to clear the HSI14RDYF flag.
        // 0: No effect 1: Clear HSI14RDYF flag
        hsi14_ready = RCC_CIR_HSI14RDYC,

        // PLLRDYC:PLL ready interrupt clear
        // This bit is set by software to clear the PLLRDYF flag.
        // 0: No effect 1: Clear PLLRDYF flag
        pll_ready = RCC_CIR_PLLRDYC,

        // HSERDYC:HSEreadyinterruptclear
        // This bit is set by software to clear the HSERDYF flag.
        // 0: No effect 1: Clear HSERDYF flag
        hse_ready = RCC_CIR_HSERDYC,

        // HSIRDYC:HSIreadyinterruptclear
        // This bit is set software to clear the HSIRDYF flag.
        // 0: No effect 1: Clear HSIRDYF flag
        hsi_ready = RCC_CIR_HSIRDYC,

        // LSERDYC:LSE ready interrupt clear
        // This bit is set by software to clear the LSERDYF flag.
        // 0: No effect 1: LSERDYF cleared
        lse_ready = RCC_CIR_LSERDYC,

        // LSIRDYC:LSI ready interrupt clear
        // This bit is set by software to clear the LSIRDYF flag.
        // 0: No effect 1: LSIRDYF cleared
        lsi_ready = RCC_CIR_LSIRDYC
    };
    void clear(intr_clear fl) { base->CIR |= (uint32_t)fl; }

    enum class intr_enable : uint32_t {
        // HSI14RDYIE:HSI14 ready interrupt enable
        // Set and cleared by software to enable/disable interrupt caused by the HSI14 oscillator stabilization.
        // 0: HSI14 ready interrupt disabled 1: HSI14 ready interrupt enabled
        hsi14 = RCC_CIR_HSI14RDYIE,

        // PLLRDYIE:PLL ready interrupt enable
        // Set and cleared by software to enable/disable interrupt caused by PLL lock.
        // 0: PLL lock interrupt disabled 1: PLL lock interrupt enabled
        pll = RCC_CIR_PLLRDYIE,

        // HSERDYIE:HSE ready interrupt enable
        // Set and cleared by software to enable/disable interrupt caused by the HSE oscillator stabilization.
        // 0: HSE ready interrupt disabled 1: HSE ready interrupt enabled
        hse = RCC_CIR_HSERDYIE,

        // HSIRDYIE:HSI ready interrupt enable
        // Set and cleared by software to enable/disable interrupt caused by the HSI oscillator stabilization.
        // 0: HSI ready interrupt disabled 1: HSI ready interrupt enabled
        hsi = RCC_CIR_HSIRDYIE,

        // LSERDYIE:LSEready interrupt enable
        // Set and cleared by software to enable/disable interrupt caused by the LSE oscillator stabilization.
        // 0: LSE ready interrupt disabled 1: LSE ready interrupt enabled
        lse = RCC_CIR_LSERDYIE,

        // LSIRDYIE:LSIready interrupt enable
        // Set and cleared by software to enable/disable interrupt caused by the LSI oscillator stabilization.
        // 0: LSI ready interrupt disabled 1: LSI ready interrupt enabled
        lsi = RCC_CIR_LSIRDYIE,

        none = 0,
        all = RCC_CIR_HSI14RDYIE | RCC_CIR_PLLRDYIE | RCC_CIR_HSERDYIE | RCC_CIR_HSIRDYIE | RCC_CIR_LSERDYIE | RCC_CIR_LSIRDYIE
    };
    void enable(intr_enable enable, intr_enable disable = intr_enable::all) { base->CIR = base->CIR & (uint32_t)disable | (uint32_t)enable; }
    bool is_enabled(intr_enable intr) { return base->CIR & (uint32_t)intr; }

    enum class intr_ready : uint32_t {
        // CSSF: Clock security system interrupt flag
        // Set by hardware when a failure is detected in the HSE oscillator. Cleared by software setting the CSSC bit.
        // 0: No clock security interrupt caused by HSE clock failure 1: Clock security interrupt caused by HSE clock failure
        clock_security = RCC_CIR_CSSF,

        // HSI14RDYF:HSI14 ready interrupt flag
        // Set by hardware when the HSI14 becomes stable and HSI14RDYDIE is set in a response to setting the HSI14ON bit in Clock control register 2 (RCC_CR2). When HSI14ON is not set but the HSI14 oscillator is enabled by the peripheral through a clock request, this bit is not set and no interrupt is generated. Cleared by software setting the HSI14RDYC bit.
        // 0: No clock ready interrupt caused by the HSI14 oscillator 1: Clock ready interrupt caused by the HSI14 oscillator
        hsi14 = RCC_CIR_HSI14RDYF,

        // PLLRDYF:PLLready interrupt flag
        // Set by hardware when the PLL locks and PLLRDYDIE is set. Cleared by software setting the PLLRDYC bit.
        // 0: No clock ready interrupt caused by PLL lock 1: Clock ready interrupt caused by PLL lock
        pll = RCC_CIR_PLLRDYF,

        // HSERDYF:HSE ready interrupt flag
        // Set by hardware when the HSE clock becomes stable and HSERDYDIE is set. Cleared by software setting the HSERDYC bit.
        // 0: No clock ready interrupt caused by the HSE oscillator 1: Clock ready interrupt caused by the HSE oscillator
        hse = RCC_CIR_HSERDYF,

        // HSIRDYF:HSI ready interrupt flag
        // Set by hardware when the HSI clock becomes stable and HSIRDYDIE is set in a response to setting the HSION (refer to Clock control register (RCC_CR)). When HSION is not set but the HSI oscillator is enabled by the peripheral through a clock request, this bit is not set and no interrupt is generated. Cleared by software setting the HSIRDYC bit.
        // 0: No clock ready interrupt caused by the HSI oscillator 1: Clock ready interrupt caused by the HSI oscillator
        hsi = RCC_CIR_HSIRDYF,

        // LSERDYF:LSE ready interrupt flag
        // Set by hardware when the LSE clock becomes stable and LSERDYDIE is set. Cleared by software setting the LSERDYC bit.
        // 0: No clock ready interrupt caused by the LSE oscillator 1: Clock ready interrupt caused by the LSE oscillator
        lse = RCC_CIR_LSERDYF,

        // LSIRDYF:LSI ready interrupt flag
        // Set by hardware when the LSI clock becomes stable and LSIRDYDIE is set. Cleared by software setting the LSIRDYC bit.
        // 0: No clock ready interrupt caused by the LSI oscillator 1: Clock ready interrupt caused by the LSI oscillator
        lsi = RCC_CIR_LSIRDYF
    };

    bool is_ready(intr_ready mask) { return base->CIR & (uint32_t)mask; }

    enum class apb2_reset : uint32_t {
        // APB peripheral reset register 2 (RCC_APB2RSTR)
        // DBGMCURST:Debug MCU reset Set and cleared by software.
        // 0: No effect 1: Reset Debug MCU
        debug_mcu = RCC_APB2RSTR_DBGMCURST,

        // TIM17RST:TIM17timerreset Set and cleared by software.
        // 0: No effect 1: Reset TIM17 timer
        tim17 = RCC_APB2RSTR_TIM17RST,

        // TIM16RST:TIM16timerreset Set and cleared by software.
        // 0: No effect 1: Reset TIM16 timer
        tim16 = RCC_APB2RSTR_TIM16RST,

        #ifdef RCC_APB2RSTR_TIM15RST
        // TIM15RST:TIM15timerreset Set and cleared by software.
        // 0: No effect 1: Reset TIM15 timer
        tim15 = RCC_APB2RSTR_TIM15RST,
        #endif

        // USART1RST:USART1reset Set and cleared by software.
        // 0: No effect 1: Reset USART1
        usart1 = RCC_APB2RSTR_USART1RST,

        // SPI1RST:SPI1 reset Set and cleared by software.
        // 0: No effect 1: Reset SPI1
        spi1 = RCC_APB2RSTR_SPI1RST,

        // TIM1RST:TIM1 timer reset Set and cleared by software.
        // 0: No effect 1: Reset TIM1 timer
        tim1 = RCC_APB2RSTR_TIM1RST,

        // ADCRST:ADCinterfacereset Set and cleared by software.
        // 0: No effect 1: Reset ADC interface
        adc = RCC_APB2RSTR_TIM17RST,

        // Bit5 USART6RST:USART6reset Set and cleared by software
        // 0: No effect 1: Reset USART6
        usart6 = RCC_APB2RSTR_TIM17RST,

        // Bit0 SYSCFGRST:SYSCFGreset Set and cleared by software.
        // 0: No effect 1: Reset SYSCFG
        syscfg = RCC_APB2RSTR_TIM17RST
    };
    void reset(apb2_reset reset) { base->APB2RSTR |= (uint32_t)reset; }

    // APB peripheral reset register 1 (RCC_APB1RSTR)
    // Access: no wait state, word, half-word and byte access
    enum class apb1_reset : uint32_t {
        // PWRRST:Power interface reset Set and cleared by software.
        // 0: No effect 1: Reset power interface
        power = RCC_APB1RSTR_PWRRST,
        #ifdef RCC_APB1RSTR_USBRST
        // USBRST:USB interface reset Set and cleared by software.
        // 0: No effect 1: Reset USB interface
        usb = RCC_APB1RSTR_USBRST,
        #endif
        #ifdef RCC_APB1RSTR_I2C2RST
        // I2C2RST:I2C2reset Set and cleared by software.
        // 0: No effect 1: Reset I2C2
        i2c2 = RCC_APB1RSTR_I2C2RST,
        #endif
        // I2C1RST:I2C1reset
        // 0: No effect 1: Reset I2C1
        i2c1 = RCC_APB1RSTR_I2C1RST,
        #ifdef RCC_APB1RSTR_USART5RST
        // USART5RST:USART5reset Set and cleared by software.
        // 0: No effect 1: Reset USART4
        usart5 = RCC_APB1RSTR_USART5RST,
        #endif
        #ifdef RCC_APB1RSTR_USART4RST
        // USART4RST:USART4reset Set and cleared by software.
        // 0: No effect 1: Reset USART4
        usart4 = RCC_APB1RSTR_USART4RST,
        #endif
        #ifdef RCC_APB1RSTR_USART3RST
        // USART3RST:USART3reset Set and cleared by software.
        // 0: No effect 1: Reset USART3
        usart3 = RCC_APB1RSTR_USART3RST,
        #endif
        #ifdef RCC_APB1RSTR_USART2RST
        // USART2RST:USART2reset Set and cleared by software.
        // 0: No effect 1: Reset USART2
        usart2 = RCC_APB1RSTR_USART2RST,
        #endif
        #ifdef RCC_APB1RSTR_SPI2RST
        // SPI2RST:SPI2reset Set and cleared by software.
        // 0: No effect 1: Reset SPI2
        spi2 = RCC_APB1RSTR_SPI2RST,
        #endif
        // WWDGRST:Window watchdog reset Set and cleared by software.
        // 0: No effect 1: Reset window watchdog
        wwdg = RCC_APB1RSTR_WWDGRST,
        // TIM14RST:TIM14 timer reset Set and cleared by software.
        // 0: No effect 1: Reset TIM14
        tim14 = RCC_APB1RSTR_TIM14RST,
        #ifdef RCC_APB1RSTR_TIM7
        // TIM7RST:TIM7 timer reset Set and cleared by software.
        // 0: No effect 1: Reset TIM7
        tim7 = RCC_APB1RSTR_TIM7,
        #endif
        #ifdef RCC_APB1RSTR_TIM6
        // TIM6RST:TIM6timerreset Set and cleared by software.
        // 0: No effec 1: Reset TIM6
        tim6 = RCC_APB1RSTR_TIM6,
        #endif
        #ifdef RCC_APB1RSTR_TIM3
        // TIM3RST:TIM3 timer reset Set and cleared by software.
        // 0: No effect 1: Reset TIM3
        tim3 = RCC_APB1RSTR_TIM3
        #endif
    };
    void reset(apb1_reset reset) { base->APB1RSTR |= (uint32_t)reset; }

    // AHB peripheral clock enable register (RCC_AHBENR)
    // Address offset: 0x14
    // Reset value: 0x0000 0014
    // Access: no wait state, word, half-word and byte access
    enum class ahb : uint32_t {
        // IOPFEN:I/O port F clock enable Set and cleared by software.
        // 0: I/O port F clock disabled 1: I/O port F clock enabled
        portf = RCC_AHBENR_GPIOFEN,

        // IOPDEN:I/OportD clock enable Set and cleared by software.
        // 0: I/O port D clock disabled 1: I/O port D clock enabled
        portd = RCC_AHBENR_GPIODEN,

        // IOPCEN:I/OportC clock enable Set and cleared by software.
        // 0: I/O port C clock disabled 1: I/O port C clock enabled
        portc = RCC_AHBENR_GPIOCEN,

        // IOPBEN:I/OportBclockenable Set and cleared by software.
        // 0: I/O port B clock disabled 1: I/O port B clock enabled
        portb = RCC_AHBENR_GPIOBEN,

        // IOPAEN:I/OportAclockenable Set and cleared by software.
        // 0: I/O port A clock disabled 1: I/O port A clock enabled
        porta = RCC_AHBENR_GPIOAEN,

        // CRCEN:CRC clock enable Set and cleared by software.
        // 0: CRC clock disabled 1: CRC clock enabled
        crc = RCC_AHBENR_CRCEN,

        // FLITFEN:FLITF clock enable
        // Set and cleared by software to disable/enable FLITF clock during Sleep mode.
        // 0: FLITF clock disabled during Sleep mode 1: FLITF clock enabled during Sleep mode
        flitf = RCC_AHBENR_FLITFEN,

        // SRAMEN:SRAM interface clock enable
        // Set and cleared by software to disable/enable SRAM interface clock during Sleep mode.
        // 0: SRAM interface clock disabled during Sleep mode. 1: SRAM interface clock enabled during Sleep mode
        sram = RCC_AHBENR_SRAMEN,

        // DMAEN:DMA clock enable Set and cleared by software.
        // 0: DMA clock disabled 1: DMA clock enabled
        dma = RCC_AHBENR_DMAEN,
    };

    bool is_enabled(ahb ahb) const { return base->AHBENR & ~(uint32_t)ahb; } 
    void enable(ahb ahb) { base->AHBENR |= (uint32_t)ahb; }
    void disable(ahb ahb) { base->AHBENR &= ~(uint32_t)ahb; }

    // APB peripheral clock enable register 2 (RCC_APB2ENR)
    // Address: 0x18
    // Reset value: 0x0000 0000
    // No wait states, except if the access occurs while an access to a peripheral in the APB domain is on going. In this case, wait states are inserted until the access to APB peripheral is finished.
    // When the peripheral clock is not active, the peripheral register values may not be readable by software and the returned value is always 0x0.
    enum class apb2 : uint32_t {
		// DBGMCUEN: MCU debugmoduleclockenable Set and reset by software.
		// 0: MCU debug module clock disabled 1: MCU debug module enabled
		dbgmcu = RCC_APB2ENR_DBGMCUEN,

		// TIM17EN: TIM17timerclockenable Set and cleared by software.
		// 0: TIM17 timer clock disabled 1: TIM17 timer clock enabled
		tim17 = RCC_APB2ENR_TIM17EN,

		// TIM16EN:TIM16timerclockenable Set and cleared by software.
		// 0: TIM16 timer clock disabled 1: TIM16 timer clock enabled
		tim16 = RCC_APB2ENR_TIM16EN,

#ifdef RCC_APB2ENR_TIM15EN
		// TIM15EN:TIM15timerclockenable Set and cleared by software.
		// 0: TIM15 timer clock disabled 1: TIM15 timer clock enabled
		tim15 = RCC_APB2ENR_TIM15EN,
#endif

		// USART1EN:USART1clockenable Set and cleared by software.
		// 0: USART1clock disabled 1: USART1clock enabled
		usart1 = RCC_APB2ENR_USART1EN,

		// SPI1EN:SPI1clockenable Set and cleared by software.
		// 0: SPI1 clock disabled 1: SPI1 clock enabled
		spi1 = RCC_APB2ENR_SPI1EN,

		// TIM1EN:TIM1timerclockenable Set and cleared by software.
		// 0: TIM1 timer clock disabled 1: TIM1P timer clock enabled
		tim1 = RCC_APB2ENR_TIM1EN,

		// ADCEN:ADCinterfaceclockenable Set and cleared by software.
		// 0: ADC interface disabled 1: ADC interface clock enabled
		adc = RCC_APB2ENR_ADCEN,

#ifdef RCC_APB2ENR_USART6EN
		// USART6EN:USART6clockenable Set and cleared by software.
		// 0: USART6clock disabled 1: USART6clock enabled
		usart6 = RCC_APB2ENR_USART6EN,
#endif

		// SYSCFGEN:SYSCFG clock enable Set and cleared by software.
		// 0: SYSCFG clock disabled 1: SYSCFG clock enabled
		syscfg = RCC_APB2ENR_SYSCFGEN,
    };
    bool is_enabled(apb2 apb) const { return base->APB2ENR & ~(uint32_t)apb; } 
    void enable(apb2 apb) { base->APB2ENR |= (uint32_t)apb; }
    void disable(apb2 apb) { base->APB2ENR &= ~(uint32_t)apb; }


    // APB peripheral clock enable register 1 (RCC_APB1ENR)
    // Reset value: 0x0000 0000
    // Access: word, half-word and byte access
    enum class apb1 : uint32_t {

        // PWREN:Powerinterfaceclockenable Set and cleared by software.
        // 0: Power interface clock disabled 1: Power interface clock enabled
        pwr = RCC_APB1ENR_PWREN,

#ifdef RCC_APB1ENR_USBEN
        // USBEN:USBinterfaceclockenable Set and cleared by software.
        // 0: USB interface clock disabled 1: USB interface clock enabled
        usb = RCC_APB1ENR_USBEN,
#endif
#ifdef RCC_APB1ENR_I2C2EN
        // I2C2EN:I2C2clockenable Set and cleared by software.
        // 0: I2C2 clock disabled 1: I2C2 clock enabled
        i2c2 = RCC_APB1ENR_I2C2EN,
#endif
        // I2C1EN:I2C1clockenable Set and cleared by software.
        // 0: I2C1 clock disabled 1: I2C1 clock enabled
        i2c1 = RCC_APB1ENR_I2C1EN,
#ifdef RCC_APB1ENR_USART5EN
        // USART5EN:USART5clockenable Set and cleared by software.
        // 0: USART5 clock disabled 1: USART5 clock enabled
        usart5 = RCC_APB1ENR_USART5EN,
#endif
#ifdef RCC_APB1ENR_USART4EN
        // USART4EN:USART4clockenable Set and cleared by software.
        // 0: USART4 clock disabled 1: USART4 clock enabled
        usart4 = RCC_APB1ENR_USART4EN,
#endif
#ifdef RCC_APB1ENR_USART3EN
        // USART3EN:USART3clockenable Set and cleared by software.
        // 0: USART3 clock disabled 1: USART3 clock enabled
        usart3 = RCC_APB1ENR_USART3EN,
#endif
#ifdef RCC_APB1ENR_USART2EN
        // USART2EN:USART2clockenable Set and cleared by software.
        // 0: USART2 clock disabled 1: USART2 clock enabled
        usart2 = RCC_APB1ENR_USART2EN,
#endif
#ifdef RCC_APB1ENR_SPI2EN
        // SPI2EN:SPI2clockenable Set and cleared by software.
        // 0: SPI2 clock disabled 1: SPI2 clock enabled
        spi2 = RCC_APB1ENR_SPI2EN,
#endif
        // WWDGEN:Windowwatchdogclockenable Set and cleared by software.
        // 0: Window watchdog clock disabled 1: Window watchdog clock enabled
        wwdg = RCC_APB1ENR_WWDGEN,

        // TIM14EN:TIM14timerclockenable Set and cleared by software.
        // 0: TIM14 clock disabled 1: TIM14 clock enabled
        tim14 = RCC_APB1ENR_TIM14EN,

#ifdef RCC_APB1ENR_TIM7EN
        // TIM7EN:TIM7timerclockenable (notavailableon STM32F070x6,nor STM32F030x4/6/8/C devices.) Set and cleared by software.
        // 0: TIM7 clock disabled 1: TIM7 clock enabled
        tim7 = RCC_APB1ENR_TIM7EN,
#endif
#ifdef RCC_APB1ENR_TIM6EN
        // TIM6EN:TIM6timerclockenable Set and cleared by software.
        // TIM6 clock disabled 1: TIM6 clock enabled
        tim6 = RCC_APB1ENR_TIM6EN,
#endif
        // TIM3EN:TIM3timerclockenable Set and cleared by software.
        // 0: TIM3 clock disabled 1: TIM3 clock enabled
        tim3 = RCC_APB1ENR_TIM3EN,
    };
    bool is_enabled(apb1 apb) const { return base->APB1ENR & ~(uint32_t)apb; } 
    void enable(apb1 apb) { base->APB1ENR |= (uint32_t)apb; }
    void disable(apb1 apb) { base->APB1ENR &= ~(uint32_t)apb; }

// RTC domain control register (RCC_BDCR)
// Address offset: 0x20
// Reset value: 0x0000 0018, reset by RTC domain reset.
// Access: 0 ≤ wait state ≤ 3, word, half-word and byte access
// BDRST:RTCdomainsoftwarereset Set and cleared by software.
// 0: Reset not activated
// 1: Resets the entire RTC domain
// RTCEN:RTCclockenable Set and cleared by software.
// 0: RTC clock disabled 1: RTC clock enabled
// Reserved,mustbekeptatresetvalue.
// RTCSEL[1:0]:RTCclocksourceselection
// Set by software to select the clock source for the RTC. Once the RTC clock source has been selected, it cannot be changed anymore unless the RTC domain is reset. The BDRST bit can be used to reset them.
// 00: No clock
// 01: LSE oscillator clock used as RTC clock
// 10: LSI oscillator clock used as RTC clock
// 11: HSE oscillator clock divided by 32 used as RTC clock
// Bits4:3 LSEDRVLSEoscillatordrivecapability
// Set and reset by software to modulate the LSE oscillator’s drive capability. A reset of the RTC domain restores the default value.
// 00: ‘Xtal mode’ lower driving capability
// 01: ‘Xtal mode’ medium high driving capability
// 10: ‘Xtal mode’ medium low driving capability
// 11: ‘Xtal mode’ higher driving capability (reset value)
// Note: The oscillator is in Xtal mode when it is not in bypass mode.
// Bit2 LSEBYP:LSEoscillatorbypass
// Set and cleared by software to bypass oscillator in debug mode. This bit can be written only
// when the external 32 kHz oscillator is disabled. 0: LSE oscillator not bypassed
// 1: LSE oscillator bypassed
// Bit1 LSERDY:LSE oscillator ready
// Set and cleared by hardware to indicate when the external 32 kHz oscillator is stable. After the
// LSEON bit is cleared, LSERDY goes low after 6 external low-speed oscillator clock cycles. 0: LSE oscillator not ready
// 1: LSE oscillator ready
// Bit0 LSEON:LSE oscillator enable Set and cleared by software.
// 0: LSE oscillator OFF 1: LSE oscillator ON

// Control/status register (RCC_CSR)
// Address: 0x24
// Reset value: 0xXXX0 0000, reset by system Reset, except reset flags by power Reset only. Access: 0 ≤ wait state ≤ 3, word, half-word and byte access
// Wait states are inserted in case of successive accesses to this register.
// LPWRRSTF: Low-power reset flag
// Set by hardware when a Low-power management reset occurs.
// Cleared by writing to the RMVF bit.
// 0: No Low-power management reset occurred 1: Low-power management reset occurred
// For further information on Low-power management reset, refer to Low-power management reset.
// WWDGRSTF:Window watchdog reset flag
// Set by hardware when a window watchdog reset occurs. Cleared by writing to the RMVF bit.
// 0: No window watchdog reset occurred 1: Window watchdog reset occurred
// IWDGRSTF:Independent watch dog reset flag
// Set by hardware when an independent watchdog reset from VDD domain occurs.
// Cleared by writing to the RMVF bit.
// 0: No watchdog reset occurred 1: Watchdog reset occurred
// SFTRSTF:Software reset flag
// Set by hardware when a software reset occurs.
// Cleared by writing to the RMVF bit.
// 0: No software reset occurred 1: Software reset occurred
// PORRSTF:POR/PDR reset flag
// Set by hardware when a POR/PDR reset occurs. Cleared by writing to the RMVF bit.
// 0: No POR/PDR reset occurred 1: POR/PDR reset occurred

// AHB peripheral reset register (RCC_AHBRSTR)
// Address: 0x28
// Reset value: 0x0000 0000
// Access: no wait states, word, half-word and byte access
// PINRSTF:PIN reset flag
// Set by hardware when a reset from the NRST pin occurs. Cleared by writing to the RMVF bit.
// 0: No reset from NRST pin occurred 1: Reset from NRST pin occurred
// OBLRSTF:Option byte loader reset flag
// Set by hardware when a reset from the OBL occurs. Cleared by writing to the RMVF bit.
// 0: No reset from OBL occurred 1: Reset from OBL occurred
// RMVF:Removeresetflag
// Set by software to clear the reset flags including RMVF.
// 0: No effect
// 1: Clear the reset flags
// V18PWRRSTF:Resetflagofthe1.8Vdomain.
// Set by hardware when a POR/PDR of the 1.8 V domain occurred. Cleared by writing to the RMVF bit.
// 0: No POR/PDR reset of the 1.8 V domain occurred 1: POR/PDR reset of the 1.8 V domain occurred
// Reserved,mustbekeptatresetvalue.
// LSIRDY:LSIoscillatorready
// Set and cleared by hardware to indicate when the LSI oscillator is stable. After the LSION bit is
// cleared, LSIRDY goes low after 3 LSI oscillator clock cycles. 0: LSI oscillator not ready
// 1: LSI oscillator ready
// LSION:LSIoscillatorenable Set and cleared by software.
// 0: LSI oscillator OFF 1: LSI oscillator ON
// IOPFRST:I/OportFreset Set and cleared by software.
// 0: No effect
// 1: Reset I/O port F
// Reserved,mustbekeptatresetvalue.
// IOPDRST:I/OportDreset Set and cleared by software.
// 0: No effect
// 1: Reset I/O port D
// IOPCRST:I/OportCreset Set and cleared by software.
// 0: No effect
// 1: Reset I/O port C
// IOPBRST:I/OportBreset Set and cleared by software.
// 0: No effect
// 1: Reset I/O port B
// IOPARST:I/OportAreset Set and cleared by software.
// 0: No effect
// 1: Reset I/O port A

// Clock configuration register 2 (RCC_CFGR2)
// Address: 0x2C
// Reset value: 0x0000 0000
// Access: no wait states, word, half-word and byte access
// Bits3:0 PREDIV[3:0]PREDIVdivisionfactor
// These bits are set and cleared by software to select PREDIV division factor. They can be
// written only when the PLL is disabled.
// Note: Bit 0 is the same bit as bit 17 in Clock configuration register (RCC_CFGR), so modifying bit 17 Clock configuration register (RCC_CFGR) also modifies bit 0 in Clock configuration register 2 (RCC_CFGR2) (for compatibility with other STM32 products)
// 0000: PREDIV input clock not divided 0001: PREDIV input clock divided by 2 0010: PREDIV input clock divided by 3 0011: PREDIV input clock divided by 4 0100: PREDIV input clock divided by 5 0101: PREDIV input clock divided by 6 0110: PREDIV input clock divided by 7 0111: PREDIV input clock divided by 8 1000: PREDIV input clock divided by 9 1001: PREDIV input clock divided by 10 1010: PREDIV input clock divided by 11 1011: PREDIV input clock divided by 12 1100: PREDIV input clock divided by 13 1101: PREDIV input clock divided by 14 1110: PREDIV input clock divided by 15 1111: PREDIV input clock divided by 16
  
// Clock configuration register 3 (RCC_CFGR3)
// Address: 0x30
// Reset value: 0x0000 0000
// Access: no wait states, word, half-word and byte access
// ADCSW:ADCclocksourceselection
// Obsolete setting. To be kept at reset value, connecting the HSI14 clock to the ADC asynchronous clock input. Proper ADC clock selection is done inside the ADC_CFGR2 (refer to Section 12.11.5: ADC configuration register 2 (ADC_CFGR2) on page 216).
// USBSW:USBclocksourceselection
// This bit is set and cleared by software to select the USB clock source.
// 0: USB clock disabled (default)
// 1: PLL clock (PLLCLK) selected as USB clock
// I2C1SW:I2C1clocksourceselection
// This bit is set and cleared by software to select the I2C1 clock source.
// 0: HSI clock selected as I2C1 clock source (default) 1: System clock (SYSCLK) selected as I2C1 clock
// Bits1:0 USART1SW[1:0]:USART1clocksourceselection
// This bit is set and cleared by software to select the USART1 clock source.
// 00: PCLK selected as USART1 clock source (default) 01: System clock (SYSCLK) selected as USART1 clock 10: LSE clock selected as USART1 clock
// 11: HSI clock selected as USART1 clock

// Clock control register 2 (RCC_CR2)
// Address: 0x34
// Reset value: 0xXX00 XX80, where X is undefined. Access: no wait states, word, half-word and byte access
// HSI14CAL[7:0]:HSI14 clock calibration
// These bits are initialized automatically at startup.
// HSI14TRIM[4:0]:HSI14 clock trimming
// These bits provide an additional user-programmable trimming value that is added to the HSI14CAL[7:0] bits. It can be programmed to adjust to variations in voltage and temperature that influence the frequency of the HSI14.
// The default value is 16, which, when added to the HSI14CAL value, should trim the HSI14 to 14 MHz ± 1%. The trimming step is around 50 kHz between two consecutive HSI14CAL steps.
// HSI14DISHSI14 clock request from ADC disable
// Set and cleared by software.
// When set this bit prevents the ADC interface from enabling the HSI14 oscillator.
// 0: ADC interface can turn on the HSI14 oscillator
// 1: ADC interface can not turn on the HSI14 oscillator
// HSI14RDY:HSI14 clock ready flag
// Set by hardware to indicate that HSI14 oscillator is stable. After the HSI14ON bit is cleared,
// HSI14RDY goes low after 6 HSI14 oscillator clock cycles. 0: HSI14 oscillator not ready
// 1: HSI14 oscillator ready
// HSI14ON:HSI14 clock enable Set and cleared by software.
// 0: HSI14 oscillator OFF 1: HSI14 oscillator ON

    };
 
    ENUM_FLAGS(rcc::clock_ready)
    ENUM_FLAGS(rcc::clock_on)
    ENUM_FLAGS(rcc::ahb)
    ENUM_FLAGS(rcc::apb1)
    ENUM_FLAGS(rcc::apb2)
    ENUM_FLAGS(rcc::intr_clear)
    ENUM_FLAGS(rcc::intr_enable)
    ENUM_FLAGS(rcc::intr_ready)
    ENUM_FLAGS(rcc::apb2_reset)
    ENUM_FLAGS(rcc::apb1_reset)
}}

#endif
