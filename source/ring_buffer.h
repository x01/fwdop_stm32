#ifndef RINGBUFFER_INCLUDED_FSwhWhdK_H_
#define RINGBUFFER_INCLUDED_FSwhWhdK_H_

#include <cstdint>
#include <cstddef>
#include <type_traits>
#include <algorithm>
#include "macrodef.h"

#pragma once

template<class T, size_t Size>
class ring_buffer
{
    using I = typename std::conditional<Size <= 128, uint8_t, typename std::conditional<Size <= 0x8000, uint16_t, size_t>::type>::type;
    static_assert((Size & (Size-1)) == 0, "Size must be power of 2");
    #define RB_Mod(x) ((x) & (Size-1))
    #define RB_ModS(x, sz) ((x) & (sz-1))

    std::aligned_storage<sizeof(T)> m_data[Size];

#if 0
public:
    uint8_t m_start = 0, m_size = 0;
    void clear() { m_size = 0; }
    bool full() const { return m_size == Size; }
    bool empty() const { return m_size == 0; }
    uint8_t size() const { return m_size; }
    void add(T a) {
        assert(!full());
        unsigned idx = RB_Mod(m_start + m_size);
        m_data[idx] = a;
        ++size;
    }
    T front() const { return m_data[m_start]; }
    T back() const { return m_data[RB_Mod(m_start + m_size)]; }
    void pop_back() { m_size--; }
    void pop_front() { m_start = RB_Mod(m_start + 1); m_size--; }
#else
    I m_start = 0, m_end = 0;

public:
    ring_buffer() {}
    template<class Y, size_t SizeY, std::enable_if_t<(Size >= SizeY), int> = 0>
    ring_buffer(ring_buffer<Y, SizeY> const& a) : ring_buffer() {
        size_t s = a.size();
        for (size_t i=0; i<s; ++i) {
            size_t x = RB_ModS(a.m_start + i, SizeY);
            new (m_data + i) T(a.m_data[x]);
        }
        m_end = s; 
    }
    template<class Y, size_t SizeY, std::enable_if_t<(Size >= SizeY), int> = 0>
    ring_buffer& operator=(ring_buffer<Y, SizeY> const& a) {
        for (size_t i=m_start, s=m_end; i<s; ++i)
            m_data[RB_Mod(i)].~T();
        size_t s=a.size();
        for (size_t i=0; i<s; ++i)
            new (m_data + RB_Mod(i)) T(a.m_data[RB_ModS(a.m_start + i, SizeY)]);
        m_end = s; 
        return *this;
    }
    size_t capacity() const { return Size; }
    void clear() { m_end = m_start; }
    bool full() const { return m_end - m_start >= Size; }
    bool empty() const { return m_start == m_end; }
    uint8_t size() const { return m_end - m_start; }

    void push_back(T a) {
        assert(!full());
        new (m_data + RB_Mod(m_end++)) T(a);
    }
    size_t append(T const* p, size_t sz) {
        assert(!sz <= full());
        size_t cap = capacity() - size();
        size_t me = RB_Mod(m_end), mb = RM_Mod(m_start);
        if (me < mb) {
            size_t fill = std::min(mb - me, sz);
            memcpy(m_data + me, p, fill);
            m_end += fill;
            return fill;
        } else {
            size_t f1 = std::min(Size - me, sz);
            memcpy(m_data + me, p, f1);
            me = RB_Mod(m_end += f1);
            if ((sz -= f1) >= 0) {
                size_t f2 = std::min(sz, mb);
                memcpy(m_data, p + f1, f2);
                m_end += f2;
                f1 += f2;
            }
            return f1;
        }
    }
    T front() const { return *(T*)(m_data + RB_Mod(m_start)); }
    T back() const { return *(T*)(m_data + RB_Mod(m_end - 1)); }
    void pop_back() { assert(m_end > m_start); m_end--; }
    void pop_front() { assert(m_end > m_start); m_start++; }
    T front_and_pop() { T r = front(); pop_front(); return r; }
    I start() const { return m_start; }
    I end() const { return m_end; }
    T operator[](size_t i) const { return m_data[RB_Mod(i)]; }
    T& operator[](size_t i) { return m_data[RB_Mod(i)]; }

#endif
};

#endif // RINGBUFFER_INCLUDED_FSwhWhdK_H_

