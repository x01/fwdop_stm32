#include <algorithm>
#include <cstring>
#include "ssd1306.hpp"
#include "printf.h"

ssd1306::ssd1306()
{
    memset(m_data, 0, sizeof m_data);
}

void ssd1306::init()
{
    // 0x00 - commands follow until <stop>
    // 0x80 - one commands byte follows
    // 0x40 - data follows until <stop>
    // 0xc0 - one data byte follows
	g_i2c.send( {
        0x00,
        ssd1306::cmd::display_off,
        ssd1306::cmd::mltiplex_ratio, 0x3f,
        ssd1306::cmd::display_offset, 0x0,
        ssd1306::cmd::start_line + 0,
        ssd1306::cmd::segment_remap_addr0,
        ssd1306::cmd::com_scan_dir_normal,
        ssd1306::cmd::com_pin_config, 0x12,
        ssd1306::cmd::contrast, 0x7f,
        ssd1306::cmd::addr_mode, 0x0,
        ssd1306::cmd::display_ram,
        ssd1306::cmd::display_normal,
        ssd1306::cmd::display_clock_div, 0x80,
        ssd1306::cmd::charge_pump, 0x14,
        ssd1306::cmd::display_on
    });
}

void ssd1306::clear()
{
    memset(m_data, 0, data_size_bytes);
}

void ssd1306::blit(unsigned y, void* src, size_t bytes)
{
    memcpy(m_data + y * width/8, src, bytes);
}

void ssd1306::send_buffer()
{
	stm32::p::i2c iic {I2C1};

    g_i2c.wait_finished();
    
    g_i2c.send( {0x00,
        ssd1306::cmd::col_addr, 0, width-1,
        ssd1306::cmd::page_addr, 0, height/8-1
    });

    g_i2c.send( {0x40}, i2c_com::seq);
    g_i2c.send(m_data, data_size_bytes, i2c_com::nowait);
}
