#ifndef ssd1306_INCLUDED_FSwhWhdK_H_
#define ssd1306_INCLUDED_FSwhWhdK_H_

#include <cstdint>
#include "ring_buffer.h"
#include "utils.h"
#include "i2c.hpp"

#pragma once

struct i2c_comm
{
    ring_buffer<uint8_t, 8> m_read, m_write;

    void init() {
    }

    bool write_byte(uint8_t a, int timeout_ms) {
        uint16_t start = now_ms();
        while (m_write.full() && now_ms() - start < timeout_ms)
            ;
        I2C1->TXDR = a;
        m_write.push_back(a);
        return true;
    }

    bool has_in() { return !m_read.empty(); }
    uint8_t read_byte() { return m_read.front_and_pop(); }
};

// <START> addr, Co|D/C|000000, ..., <STOP>
// addr - slave address 
// Co - 0:data 1:command
// D/C - next byte data or command. 0:command 1:data which will be stored in GDDRAM. column addr point will be increased automatically

// “b011 1100” or “b011 1101”
// Address 0x3C for 128x32
// Address 0x3D for 128x64
struct ssd1306
{
    enum cmd : uint8_t
    {
        // FUNDAMENTAL

        contrast = 0x81, // plus one byte - A[7:0] conrast. at RESET = 0x7f

        display_ram = 0xa4, // at RESET
        display_1 = 0xa5,

        display_normal = 0xa6, // at RESET. 0:off 1:on
        display_inverse = 0xa7, // 1:off 0:on

        display_off = 0xae, // sleep mode. at RESET
        display_on = 0xaf,

        // SCROLL

        // Continuous Horizontal Scroll Setup
        // +6 bytes:
        // A[7:0] dummy - 0x00
        // B[2:0] start page - 0..7
        // C[2:0] time interval in frame frq - 0:5 1:64 2:128 3:256 4:3 5:4 6:25 7:2
        // D[2:0] end pade address - 0..7
        // E[2:0] dummy - 0x00
        // F[2:0] dummy - 0xff
        scroll_right = 0x26,
        scroll_left = 0x27,
        
        // Continuous Vertical and Horizontal Scroll Setup
        scroll_vert_right = 0x29,
        scroll_vert_left = 0x2a,

        scroll_deactivate = 0x2e, // ram needs to be rewritten

        scroll_activate = 0x2f, // after 0x26, 27, 29 2a
        
        // Set Vertical Scroll Area
        // A[5:0] no of rows in top fixed area
        // B[6:0] no of rows in scroll area
        scroll_vert_area = 0xa3,

        // ADDRESSING

        // Set Lower Column Start Address for Page Addressing Mode
        // 0[3:0] lower nibble of col start - 0 at RESET
        addr_low_col = 0x0,

        // Set Higher Column Start Address for Page Addressing Mode
        // 0[3:0] higher nibble of col start - 0 at RESET
        addr_hi_col = 0x10,
        
        // Set Memory Addressing Mode
        // A[1:0] - 0:horizontal 1:vertical 2:page addressing (at RESET) 3:invalid
        addr_mode = 0x20,

        // Set Column Address
        // A[6:0] column start address, range : 0-127d, (at RESET=0d)
        // B[6:0] column end address, range : 0-127d, (RESET =127d)
        col_addr = 0x21,

        // Set Page Address
        // A[2:0] page start Address, range : 0-7d, (RESET = 0d)
        // B[2:0] page end Address, range : 0-7d, (RESET = 7d)
        // Note: This command is only for horizontal or vertical addressing mode.                
        page_addr = 0x22,
 
        // Set Page Start Address for Page Addressing Mode
        // 0[3:0] Set GDDRAM Page Start Address (PAGE0~PAGE7) for Page Addressing Mode using X[2:0].
        // Note (1) This command is only for page addressing mode
        page_start = 0xb0,

        // Charge Pump Setting
        // A[2] = 0b, Disable charge pump(RESET)
        // A[2] = 1b, Enable charge pump during display on
        // Note (1) The Charge Pump must be enabled by the following command:
        // 8Dh 14h  ; Enable Charge Pump, Charge Pump Setting
        // AFh      ; Display ON
        charge_pump = 0x8d,
        charge_pump_off = 0x89,

        // Hardware Configuration (Panel resolution & layout related) Command Table
        // 0[5:0] Set display RAM display start line register from 0-63 using X5X3X2X1X0. Display start line register is reset to 000000b during RESET.
        start_line = 0x40,

        // Set Segment Re-map
        // A0h, X[0]=0b: column address 0 is mapped to SEG0 (RESET)
        // A1h, X[0]=1b: column address 127 is mapped to SEG0
        segment_remap_addr0 = 0xA0,
        segment_remap_addr127 = 0xA1,

        // Set Multiplex Ratio
        // Set MUX ratio to N+1 MUX
        // A[5:0] from 16MUX to 64MUX, RESET=111111b (i.e. 63d, 64MUX) A[5:0] from 0 to 14 are invalid entry.
        mltiplex_ratio = 0xa8,

        // Set COM Output Scan Direction
        // C0h, X[3]=0b: normal mode (RESET) Scan from COM0 to COM[N –1]
        // C8h, X[3]=1b: remapped mode. Scan from COM[N-1] to COM0
        // Where N is the Multiplex ratio.
        com_scan_dir_normal = 0xc0,
        com_scan_dir_remapped = 0xc8,

        // Set Display Offset 
        // A[5:0] Set vertical shift by COM from 0d~63d The value is reset to 00h after RESET.
        display_offset = 0xd3,

        // Set COM Pins Hardware Configuration
        // A[5:4] - value: 0 0 A5 A4 0 0 1 0
        //   A[4]=0b, Sequential COM pin configuration A[4]=1b(RESET), Alternative COM pin configuration
        //   A[5]=0b(RESET), Disable COM Left/Right remap
        //   A[5]=1b, Enable COM Left/Right remap
        com_pin_config = 0xda,

        // Set Display Clock Divide Ratio/Oscillator Frequency
        // A[7:0] value
        //   A[3:0] : Define the divide ratio (D) of the display clocks (DCLK): Divide ratio= A[3:0] + 1, RESET is 0000b (divide ratio = 1)
        //   A[7:4] : Set the Oscillator Frequency, FOSC. Oscillator Frequency increases with the value of A[7:4] and vice versa.
        // RESET is 1000b Range:0000b~1111b
        // Frequency increases as setting value increases.
        display_clock_div = 0xd5,

        // Set Pre-charge Period
        // A[7:0] value
        //   A[3:0] : Phase 1 period of up to 15 DCLK clocks 0 is invalid entry(RESET=2h)
        //   A[7:4] : Phase 2 period of up to 15 DCLK clocks 0 is invalid entry (RESET=2h )        
        pre_charge_period = 0xd9,

        // Set VCOMH Deselect Level
        // A[6:4] Hex code
        // 000b / 00h ~ 0.65 x VCC
        // 010b / 20h ~ 0.77 x VCC (RESET)
        // 011b / 30h ~ 0.83 x VCC        
        vcom_deselect_level = 0xdb,

        // Command for no operation
        nop = 0xe3
    };

    ssd1306();

    void init();
    void send_data(uint8_t data);
    void blit(unsigned y, void* src, size_t bytes);
    void set_byte(unsigned ofs, uint8_t v) {
        m_data[ofs] = v;
    }
    void clear();
    void set_point(unsigned x, unsigned y) {
        // unsigned o = x >> 3;
        // unsigned p = x & 3;
        // unsigned offset = y * width/8 + o;
        // assert(offset < data_size_bytes);
        // m_data[offset] = 1 << p;

        unsigned b = y&0x7;
        unsigned l = y >> 3;
        unsigned offset = l*width + x;
        assert(offset < data_size_bytes);
        m_data[offset] = 1 << b;
    }

    void send_buffer();

    enum : uint32_t { width = 128, height = 64, data_size_bytes = width*height/8 };



private:
    uint8_t m_data[data_size_bytes];
};

#endif 
