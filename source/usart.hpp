#ifndef USART_INCLUDED_FSwhWhdK_H_
#define USART_INCLUDED_FSwhWhdK_H_

#include <stm32f030x6.h>

#include <cstddef>
#include "utils.h"

namespace stm32 { namespace p {

    namespace reg
    {
        struct cr1 {
            union {
                uint32_t reg;
                struct {
                    uint32_t	ue:1, reserved_1:1, re:1, te:1, idleie:1, rxneie:1, tcie:1, txeie:1, peie:1, ps:1, pce:1, wake:1, m0:1, mme:1, cmie:1, over8:1,
                                dedt:5, deat:5, rtoie:1, reserved_27:1, m1:1, reserved_31:3;
                };
            };
        };
    }

    struct usart
    {
        USART_TypeDef* base;

        usart() {}
        usart(USART_TypeDef* base) : base(base) {}

#define IMPL_ENABLE_BIT(COND, REG, FLAG) do { if (COND) base->REG |= FLAG; else base->CR1 &= ~FLAG; } while (0)

        // M1:Wordlength
        // This bit, with bit 12 (M0), determines the word length. It is set or cleared by software.
        // M[1:0] = 00: 1 Start bit, 8 data bits, n stop bits
        // M[1:0] = 01: 1 Start bit, 9 data bits, n stop bits
        // M[1:0] = 10: 1 Start bit, 7 data bits, n stop bits
        // This bit can only be written when the USART is disabled (UE=0).
        // M0:Wordlength
        // This bit determines the word length. It is set or cleared by software.
        // 0: 1 Start bit, 8 data bits, n stop bits
        // 1: 1 Start bit, 9 data bits, n stop bits
        enum class wl { s1_8bit = 0, s1_9bit = 1, s1_7bit = 2 };
        void word_length(wl l) {
            #define USART_CR1_M1_Pos 28
            #define USART_CR1_M1 (1<<USART_CR1_M1_Pos)
            uint32_t r = base->CR1 & ~(USART_CR1_M | USART_CR1_M1);
            r |= ((uint32_t)l&0x01) << USART_CR1_M_Pos;
            r |= (((uint32_t)l&0x02) >> 1) << USART_CR1_M1_Pos;
            base->CR1 = r;
        }

        // DEAT[4:0]:DriverEnable assertion time
        // This 5-bit value defines the time between the activation of the DE (Driver Enable) signal and the beginning of the start bit. It is expressed in sample time units (1/8 or 1/16 bit duration, depending on the oversampling rate).
        // This bit field can only be written when the USART is disabled (UE=0).
        // Note: If the Driver Enable feature is not supported, this bit is reserved and must be kept cleared. Please refer to Section 23.3: USART implementation on page 597.
        // DEDT[4:0]:DriverEnablede-assertiontime
        // This 5-bit value defines the time between the end of the last stop bit, in a transmitted
        // message, and the de-activation of the DE (Driver Enable) signal. It is expressed in sample time units (1/8 or 1/16 bit duration, depending on the oversampling rate).
        // If the USART_TDR register is written during the DEDT time, the new data is transmitted only when the DEDT and DEAT times have both elapsed.
        // This bit field can only be written when the USART is disabled (UE=0).
        // Note: If the Driver Enable feature is not supported, this bit is reserved and must be kept cleared. Please refer to Section 23.3: USART implementation on page 597.
        void assertion_time(unsigned assertion, unsigned deassertion)
        {
            uint32_t r = base->CR1 & ~(USART_CR1_DEAT | USART_CR1_DEDT);
            r |= assertion << USART_CR1_DEAT_Pos;
            r |= deassertion << USART_CR1_DEDT_Pos;
            base->CR1 = r;
        }

        // OVER8:Oversampling mode
        // 0: Oversampling by 16
        // 1: Oversampling by 8
        // This bit can only be written when the USART is disabled (UE=0).
        void oversampling(bool by8) { IMPL_ENABLE_BIT(by8, CR1, USART_CR1_OVER8); }

        // MME:Mute mode enable
        // This bit activates the mute mode function of the USART. when set, the USART can switch between the active and mute modes, as defined by the WAKE bit. It is set and cleared by software.
        // 0: Receiver in active mode permanently
        // 1: Receiver can switch between mute mode and active mode.
        void mute_mode(bool can_switch) { IMPL_ENABLE_BIT(can_switch, CR1, USART_CR1_MME); }

        // WAKE:Receiver wake up method
        // This bit determines the USART wakeup method from Mute mode. It is set or cleared by software.
        // 0: Idle line
        // 1: Address mark
        // This bit field can only be written when the USART is disabled (UE=0).
        void wakeup(bool address_mark) { IMPL_ENABLE_BIT(address_mark, CR1, USART_CR1_WAKE); }

        // PCE:Parity control enable
        // This bit selects the hardware parity control (generation and detection). When the parity control is enabled, the computed parity is inserted at the MSB position (9th bit if M=1; 8th bit if M=0) and parity is checked on the received data. This bit is set and cleared by software. Once it is set, PCE is active after the current byte (in reception and in transmission).
        // 0: Parity control disabled
        // 1: Parity control enabled
        // This bit field can only be written when the USART is disabled (UE=0).
        // PS: Parity selection
        // This bit selects the odd or even parity when the parity generation/detection is enabled (PCE bit set). It is set and cleared by software. The parity will be selected after the current byte.
        // 0: Even parity
        // 1: Odd parity
        // This bit field can only be written when the USART is disabled (UE=0).
        void parity(bool enable, bool odd = false) {
            uint32_t r = base->CR1 & ~(USART_CR1_PCE | USART_CR1_PS);
            r |= (uint32_t)enable << USART_CR1_PCE_Pos;
            r |= (uint32_t)odd << USART_CR1_PS_Pos;
            base->CR1 = r;
        }

        enum class intr {
            none = 0,
        // PEIE:parity error interrupt enable
        // This bit is set and cleared by software.
        // 0: Interrupt is inhibited
        // 1: A USART interrupt is generated whenever PE=1 in the USART_ISR register
            parity_err = USART_CR1_PEIE,
        // TXEIE:interrupt enable
        // This bit is set and cleared by software.
        // 0: Interrupt is inhibited
        // 1: A USART interrupt is generated whenever TXE=1 in the USART_ISR register
            tx_empty = USART_CR1_TXEIE,
        // TCIE:Transmission complete interrupt enable
        // This bit is set and cleared by software.
        // 0: Interrupt is inhibited
        // 1: A USART interrupt is generated whenever TC=1 in the USART_ISR register
            tx_complete = USART_CR1_TCIE,
        // RXNEIE:RXNE interrupt enable
        // This bit is set and cleared by software.
        // 0: Interrupt is inhibited
        // 1: A USART interrupt is generated whenever ORE=1 or RXNE=1 in the USART_ISR register
            rx_not_empty = USART_CR1_RXNEIE,
        // IDLEIE:IDLE interrupt enable
        // This bit is set and cleared by software.
        // 0: Interrupt is inhibited
        // 1: A USART interrupt is generated whenever IDLE=1 in the USART_ISR register
            idle = USART_CR1_IDLEIE,

        // CMIE:Character match interrupt enable
        // This bit is set and cleared by software.
        // 0: Interrupt is inhibited
        // 1: A USART interrupt is generated when the CMF bit is set in the USART_ISR register.
            char_match = USART_CR1_CMIE,

            // RTOIE:Receiver timeout interrupt enable
            // This bit is set and cleared by software.
            // 0: Interrupt is inhibited
            // 1: An USART interrupt is generated when the RTOF bit is set in the USART_ISR register.
            // Note: If the USART does not support the Receiver timeout feature, this bit is reserved and forced by hardware to ‘0’. Section 23.3: USART implementation on page 597.
            rx_timeout = USART_CR1_RTOIE,
            all = parity_err | tx_empty | tx_complete | rx_timeout | rx_not_empty | idle | char_match
        };
        void ie(intr enable, intr disable = intr::all) {
            uint32_t r = base->CR1;
            r &= (uint32_t)disable;
            r |= (uint32_t)enable;
            base->CR1 = r;
        }

        // TE:Transmitter enable
        // This bit enables the transmitter. It is set and cleared by software. 0: Transmitter is disabled
        // 1: Transmitter is enabled
        // During transmission, a “0” pulse on the TE bit (“0” followed by “1”) sends a preamble (idle line) after the current word. In order to generate an idle character, the TE must not be immediately written to 1.
        void transmit_enable(bool enable) { IMPL_ENABLE_BIT(enable, CR1, USART_CR1_TE); }

        // RE:Receiver enable
        // This bit enables the receiver. It is set and cleared by software. 0: Receiver is disabled
        // 1: Receiver is enabled and begins searching for a start bit
        // Reserved,mustbekeptatresetvalue.
        void receive_enable(bool enable) { IMPL_ENABLE_BIT(enable, CR1, USART_CR1_RE); }

        enum class rxtx : uint32_t { none = 0, tx = USART_CR1_TE, rx = USART_CR1_RE, all = USART_CR1_TE|USART_CR1_RE };
        void enable(rxtx recv_transmit = rxtx::all)
        {
            base->CR1 = base->CR1 & ~(uint32_t)rxtx::all | (uint32_t)recv_transmit;
        }

        // UE:USARTenable
        // When this bit is cleared, the USART prescalers and outputs are stopped immediately, and current operations are discarded. The configuration of the USART is kept, but all the status flags, in the USART_ISR are set to their default values. This bit is set and cleared by software.
        // 0: USART prescaler and outputs disabled, low-power mode
        // 1: USART enabled
        // Note: In order to go into low-power mode without generating errors on the line, the TE bit must be reset before and the software must wait for the TC bit in the USART_ISR to be set before resetting the UE bit.
        // The DMA requests are also reset when UE = 0 so the DMA channel must be disabled before resetting the UE bit.
        void enable(bool enable) { IMPL_ENABLE_BIT(enable, CR1, USART_CR1_UE); }

        // Control register 2 (USART_CR2)
        // Reset value: 0x0000

        // ADDM7:7-bit Address Detection/4-bit Address Detection
        // This bit is for selection between 4-bit address detection or 7-bit address detection.
        // 0: 4-bit address detection
        // 1: 7-bit address detection (in 8-bit data mode)
        // This bit can only be written when the USART is disabled (UE=0)
        // Note: In 7-bit and 9-bit data modes, the address detection is done on 6-bit and 8-bit address (ADD[5:0] and ADD[7:0]) respectively.
        void addr_detect(bool bit7detect, uint8_t address) {
            uint32_t r = base->CR2;
            r &= ~(USART_CR2_ADD | USART_CR2_ADDM7);
            r |= (uint32_t)bit7detect << USART_CR2_ADDM7;
            r |= (address << USART_CR2_ADD_Pos) & USART_CR2_ADD_Msk;
            base->CR2 = r;
        }

        // ABRMOD[1:0]:Auto baud rate mode
        // These bits are set and cleared by software.
        // 00: Measurement of the start bit is used to detect the baud rate.
        // 01: Falling edge to falling edge measurement. (the received frame must start with a single bit = 1 -> Frame = Start10xxxxxx)
        // 10: 0x7F frame detection.
        // 11: 0x55 frame detection
        // This bit field can only be written when ABREN = 0 or the USART is disabled (UE=0).
        // Note:If DATAINV=1 and/or MSBFIRST=1 the patterns must be the same on the line, for example 0xAA for MSBFIRST)
        void auto_baud(bool enable, unsigned mode) {
            uint32_t r = base->CR2;
            if (enable) {
                r &= ~(USART_CR2_ABRMODE_Msk);
                r |= (mode << USART_CR2_ABRMODE_Pos) & USART_CR2_ABRMODE_Msk;
                r |= USART_CR2_ABREN;
            } else
                r &= ~USART_CR2_ABREN;
            base->CR2 = r;
        }

        // MSBFIRST:Most significant bit first
        // This bit is set and cleared by software.
        // 0: data is transmitted/received with data bit 0 first, following the start bit.
        // 1: data is transmitted/received with the MSB (bit 7/8/9) first, following the start bit. This bit field can only be written when the USART is disabled (UE=0).
        void msb_first(bool enable) { IMPL_ENABLE_BIT(enable, CR2, USART_CR2_MSBFIRST); }

        // DATAINV:Binary data inversion
        // This bit is set and cleared by software.
        // 0: Logical data from the data register are send/received in positive/direct logic. (1=H, 0=L)
        // 1: Logical data from the data register are send/received in negative/inverse logic. (1=L, 0=H). The parity bit is also inverted.
        // This bit field can only be written when the USART is disabled (UE=0).
        void data_invert(bool enable) { IMPL_ENABLE_BIT(enable, CR2, USART_CR2_DATAINV); }

        // TXINV:TX pin active level inversion This bit is set and cleared by software.
        // 0: TX pin signal works using the standard logic levels (VDD =1/idle, Gnd=0/mark) 1: TX pin signal values are inverted. (VDD =0/mark, Gnd=1/idle).
        // This allows the use of an external inverter on the TX line.
        // This bit field can only be written when the USART is disabled (UE=0).
        void tx_invert(bool enable) { IMPL_ENABLE_BIT(enable, CR2, USART_CR2_TXINV); }

        // RXINV:RX pin active level inversion This bit is set and cleared by software.
        // 0: RX pin signal works using the standard logic levels (VDD =1/idle, Gnd=0/mark) 1: RX pin signal values are inverted. (VDD =0/mark, Gnd=1/idle).
        // This allows the use of an external inverter on the RX line.
        // This bit field can only be written when the USART is disabled (UE=0).
        void rx_invert(bool enable) { IMPL_ENABLE_BIT(enable, CR2, USART_CR2_RXINV); }

        // SWAP:SwapTX/RXpins
        // This bit is set and cleared by software.
        // 0: TX/RX pins are used as defined in standard pinout
        // 1: The TX and RX pins functions are swapped. This allows to work in the case of a cross-wired connection to another USART.
        // This bit field can only be written when the USART is disabled (UE=0).
        void swap_txrx(bool enable) { IMPL_ENABLE_BIT(enable, CR2, USART_CR2_SWAP); }

        // STOP[1:0]:STOPbits
        // These bits are used for programming the stop bits.
        // This bit field can only be written when the USART is disabled (UE=0).
        // 00: 1 stop bit
        // 01: Reserved
        // 10: 2 stop bits
        // 11: Reserved
        enum class stop_bits { bit1 = 0, bit2 = 2 };
        void stop_bits(stop_bits stopbits) { base->CR2 = (base->CR2 & ~USART_CR2_STOP_Msk) | ((uint32_t)stopbits << USART_CR2_STOP_Pos); }

        // CLKEN:Clock enable
        // This bit allows the user to enable the CK pin.
        // 0: CK pin disabled
        // 1: CK pin enabled
        // This bit can only be written when the USART is disabled (UE=0).
        // Note: If synchronous mode is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 23.3: USART implementation on page 597.
        // CPOL:Clock polarity
        // This bit allows the user to select the polarity of the clock output on the CK pin in synchronous mode. It works in conjunction with the CPHA bit to produce the desired clock/data relationship
        // 0: Steady low value on CK pin outside transmission window
        // 1: Steady high value on CK pin outside transmission window
        // This bit can only be written when the USART is disabled (UE=0).
        // Note: If synchronous mode is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 23.3: USART implementation on page 597.
        // CPHA:Clock phase
        // This bit is used to select the phase of the clock output on the CK pin in synchronous mode. It works in conjunction with the CPOL bit to produce the desired clock/data relationship (see Figure 236 and Figure 237)
        // 0: The first clock transition is the first data capture edge
        // 1: The second clock transition is the first data capture edge
        // This bit can only be written when the USART is disabled (UE=0).
        // Note: If synchronous mode is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 23.3: USART implementation on page 597.
        // LBCL:Last bit clock pulse
        // This bit is used to select whether the clock pulse associated with the last data bit transmitted (MSB) has to be output on the CK pin in synchronous mode.
        // 0: The clock pulse of the last data bit is not output to the CK pin
        // 1: The clock pulse of the last data bit is output to the CK pin
        // Caution: Thelastbitisthe7thor8thor9thdatabittransmitteddependingonthe7or8or9bit format selected by the M bit in the USART_CR1 register.
        // This bit can only be written when the USART is disabled (UE=0).
        // Note: If synchronous mode is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 23.3: USART implementation on page 597.
        void clock(bool enable, bool polarity_high = false, bool clock_phase_second = false, bool pulse_to_ck = false) {
            uint32_t r = base->CR2;
            if (enable) {
                r &= ~(USART_CR2_CPOL_Msk | USART_CR2_CPHA_Msk | USART_CR2_LBCL_Msk);
                if (polarity_high) r |= USART_CR2_CPOL;
                if (clock_phase_second) r |= USART_CR2_CPHA;
                if (pulse_to_ck) r |= USART_CR2_LBCL;
                r |= USART_CR2_CLKEN;
            } else
                r &= ~USART_CR2_CLKEN;
            base->CR2 = r;
        }

        // Control register 3 (USART_CR3)
        // Reset value: 0x0000
        enum conf {
            // DEP:Driver enable polarity selection
            // 0: DE signal is active high.
            // 1: DE signal is active low.
            // This bit can only be written when the USART is disabled (UE=0).
            // Note: If the Driver Enable feature is not supported, this bit is reserved and must be kept cleared. Please refer to Section 23.3: USART implementation on page 597.
            driver_polarity_low = USART_CR3_DEP,
            // DEM:Driver enable mode
            // This bit allows the user to activate the external transceiver control, through the DE signal.
            // 0: DE function is disabled. 
            // 1: DE function is enabled. The DE signal is output on the RTS pin. This bit can only be written when the USART is disabled (UE=0).
            // Note: If the Driver Enable feature is not supported, this bit is reserved and must be kept cleared. Section 23.3: USART implementation on page 597.
            driver_enable = USART_CR3_DEM,
            // DDRE:DMA Disable on Reception Error
            // 0: DMA is not disabled in case of reception error. The corresponding error flag is set but RXNE is kept 0 preventing from overrun. As a consequence, the DMA request is not asserted, so the erroneous data is not transferred (no DMA request), but next correct received data will be transferred .
            // 1: DMA is disabled following a reception error. The corresponding error flag is set, as well as RXNE. The DMA request is masked until the error flag is cleared. This means that the software must first disable the DMA request (DMAR = 0) or clear RXNE before clearing the error flag.
            // This bit can only be written when the USART is disabled (UE=0).
            // Note: The reception errors are: parity error, framing error or noise error.
            dma_reception_error_enable = USART_CR3_DDRE,
            // OVRDIS:Overrun Disable
            // This bit is used to disable the receive overrun detection.
            // 0: Overrun Error Flag, ORE, is set when received data is not read before receiving new data.
            // 1: Overrun functionality is disabled. If new data is received while the RXNE flag is still set the ORE flag is not set and the new received data overwrites the previous content of the USART_RDR register.
            // This bit can only be written when the USART is disabled (UE=0).
            // Note: This control bit allows checking the communication flow without reading the data.
            overrun_disable = USART_CR3_OVRDIS,
            // ONEBIT:One sample bit method enable
            // This bit allows the user to select the sample method. When the one sample bit method is selected the noise detection flag (NF) is disabled.
            // 0: Three sample bit method
            // 1: One sample bit method
            // This bit can only be written when the USART is disabled (UE=0).
            // Note: ONEBIT feature applies only to data bits, It does not apply to Start bit.
            onebit = USART_CR3_ONEBIT,
            // CTSIE:CTS interrupt enable
            // 0: Interrupt is inhibited
            // 1: An interrupt is generated whenever CTSIF=1 in the USART_ISR register
            // Note: If the hardware flow control feature is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 23.3: USART implementation on page 597.
            cts_intr = USART_CR3_CTSIE,
            // CTSE:CTS enable
            // 0: CTS hardware flow control disabled
            // 1: CTS mode enabled, data is only transmitted when the CTS input is asserted (tied to 0). If the CTS input is de-asserted while data is being transmitted, then the transmission is completed before stopping. If data is written into the data register while CTS is de-asserted, the transmission is postponed until CTS is asserted.
            // This bit can only be written when the USART is disabled (UE=0)
            // Note: If the hardware flow control feature is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 23.3: USART implementation on page 597.
            cts = USART_CR3_CTSE,
            // RTSE:RTS enable
            // 0: RTS hardware flow control disabled
            // 1: RTS output enabled, data is only requested when there is space in the receive buffer. The transmission of data is expected to cease after the current character has been transmitted. The RTS output is asserted (pulled to 0) when data can be received.
            // This bit can only be written when the USART is disabled (UE=0).
            // Note: If the hardware flow control feature is not supported, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 23.3: USART implementation on page 597.
            rts = USART_CR3_RTSE,
            // DMAT:DMA enable transmitter
            // This bit is set/reset by software
            // 1: DMA mode is enabled for transmission
            // 0: DMA mode is disabled for transmission
            dma_for_tx = USART_CR3_DMAT,
            // DMAR:DMA enable receiver
            // This bit is set/reset by software
            // 1: DMA mode is enabled for reception
            // 0: DMA mode is disabled for reception
            dma_for_rx = USART_CR3_DMAR,
            // HDSEL:Half-duplex selection
            // Selection of Single-wire Half-duplex mode
            // 0: Half duplex mode is not selected
            // 1: Half duplex mode is selected
            // This bit can only be written when the USART is disabled (UE=0).
            half_duplex = USART_CR3_HDSEL,
            // EIE:Error interrupt enable
            // Error Interrupt Enable Bit is required to enable interrupt generation in case of a framing error, overrun error or noise flag (FE=1 or ORE=1 or NF=1 in the USART_ISR register). 0: Interrupt is inhibited
            // 1: An interrupt is generated when FE=1 or ORE=1 or NF=1 in the USART_ISR register.
            error_intr = USART_CR3_EIE,
        };
        void enable(conf fl) { base->CR3 |= (uint32_t)fl; }
        void disable(conf fl) { base->CR3 &= (uint32_t)fl; }

        // Baud rate register (USART_BRR)
        // Reset value: 0x0000
        // baud = fCK / (16*USARTDIV)
        // hence, brr = (APBCLK / 16) / baud
        void baud_rate(unsigned usartdiv, bool over8) {
            unsigned brr;
            if (over8)
                brr = (usartdiv & ~0x000f) | ((usartdiv & 0x0f) >> 1);
            else
                brr = usartdiv;
            base->BRR = brr;
        }

        // RTOEN: Receiver time out enable
        // This bit is set and cleared by software.
        // 0: Receiver timeout feature disabled.
        // 1: Receiver timeout feature enabled.
        // When this feature is enabled, the RTOF flag in the USART_ISR register is set if the RX line is idle (no reception) for the duration programmed in the RTOR (receiver timeout register).
        // RTO[23:0]:Receiver timeout value
        // This bit-field gives the Receiver timeout value in terms of number of bit duration.
        // In standard mode, the RTOF flag is set if, after the last received character, no new start bit is detected for more than the RTO value.
        // Note: This value must only be programmed once per received character.
        // Note: RTOR can be written on the fly. If the new value is lower than or equal to the counter, the RTOF flag is set.
        // This register is reserved and forced by hardware to “0x00000000” when the Receiver timeout feature is not supported. Please refer to Section 23.3: USART implementation on page 597.
        void recv_timeout(bool enable, unsigned value) {
            IMPL_ENABLE_BIT(enable, CR2, USART_CR2_RTOEN);
            if (enable)
                base->RTOR = value & USART_RTOR_RTO_Msk;
        }

        // Request register (USART_RQR)
        // Reset value: 0x0000
        // RXFRQ:Receive data flush request
        // Writing 1 to this bit clears the RXNE flag.
        // This allows to discard the received data without reading it, and avoid an overrun condition.
        // MMRQ:Mute mode request
        // Writing 1 to this bit puts the USART in mute mode and sets the RWU flag.
        // SBKRQ:Send break request
        // Writing 1 to this bit sets the SBKF flag and request to send a BREAK on the line, as soon as the transmit machine is available.
        // Note: In the case the application needs to send the break character following all previously inserted data, including the ones not yet transmitted, the software should wait for the TXE flag assertion before setting the SBKRQ bit.
        // ABRRQ:Auto baud rate request
        // Writing 1 to this bit resets the ABRF flag in the USART_ISR and request an automatic baud
        // rate measurement on the next received data frame.
        // Note: If the USART does not support the auto baud rate feature, this bit is reserved and forced by hardware to ‘0’. Please refer to Section 23.3: USART implementation on page 597.
        enum class request : uint32_t {
            data_flush = USART_RQR_RXFRQ, mute_mode = USART_RQR_MMRQ,
            send_break = USART_RQR_SBKRQ, auto_baud = USART_RQR_ABRRQ,
            all = USART_RQR_RXFRQ | USART_RQR_MMRQ | USART_RQR_SBKRQ | USART_RQR_ABRRQ
        };
        void send_request(request r) { base->RQR = (uint32_t)r & ~(uint32_t)request::all; }

        enum class isr {
            // RWU:ReceiverwakeupfromMutemode
            // This bit indicates if the USART is in mute mode. It is cleared/set by hardware when a wakeup/mute sequence is recognized. The mute mode control sequence (address or IDLE) is selected by the WAKE bit in the USART_CR1 register.
            // When wakeup on IDLE mode is selected, this bit can only be set by software, writing 1 to the MMRQ bit in the USART_RQR register.
            // 0: Receiver in active mode
            // 1: Receiver in mute mode
            wakeup = USART_ISR_RWU,

            // SBKF:Sendbreak flag
            // This bit indicates that a send break character was requested. It is set by software, by writing 1 to the SBKRQ bit in the USART_RQR register. It is automatically reset by hardware during the stop bit of break transmission.
            // 0: No break character is transmitted
            // 1: Break character will be transmitted            
            send_break = USART_ISR_SBKF,

            // CMF:Character match flag
            // This bit is set by hardware, when the character defined by ADD[7:0] is received. It is cleared by software, writing 1 to the CMCF in the USART_ICR register.
            // An interrupt is generated if CMIE=1in the USART_CR1 register.
            // 0: No Character match detected
            // 1: Character Match detected
            char_match = USART_ISR_CMF,

            // BUSY:Busyflag
            // This bit is set and reset by hardware. It is active when a communication is ongoing on the RX line (successful start bit detected). It is reset at the end of the reception (successful or not).
            // 0: USART is idle (no reception)
            // 1: Reception on going            
            busy = USART_ISR_BUSY,

            // ABRF:Auto baudr ate flag
            // This bit is set by hardware when the automatic baud rate has been set (RXNE will also be
            // set, generating an interrupt if RXNEIE = 1) or when the auto baud rate operation was completed without success (ABRE=1) (ABRE, RXNE and FE are also set in this case)
            // It is cleared by software, in order to request a new auto baud rate detection, by writing 1 to the ABRRQ in the USART_RQR register.
            // Note: If the USART does not support the auto baud rate feature, this bit is reserved and forced by hardware to ‘0’.            
            auto_baud = USART_ISR_ABRF,

            // ABRE:Autobaud rate error
            // This bit is set by hardware if the baud rate measurement failed (baud rate out of range or character comparison failed)
            // It is cleared by software, by writing 1 to the ABRRQ bit in the USART_CR3 register.
            // Note: If the USART does not support the auto baud rate feature, this bit is reserved and forced by hardware to ‘0’.
             auto_baud_error = USART_ISR_ABRE,

            // RTOF:Receiver timeout
            // This bit is set by hardware when the timeout value, programmed in the RTOR register has
            // lapsed, without any communication. It is cleared by software, writing 1 to the RTOCF bit in the USART_ICR register.
            // An interrupt is generated if RTOIE=1 in the USART_CR1 register.
            // 0: Timeout value not reached
            // 1: Timeout value reached without any data reception
            // Note:
            // If a time equal to the value programmed in RTOR register separates 2 characters, RTOF is not set. If this time exceeds this value + 2 sample times (2/16 or 2/8, depending on the oversampling method), RTOF flag is set.
            // The counter counts even if RE = 0 but RTOF is set only when RE = 1. If the timeout has already elapsed when RE is set, then RTOF will be set.
            // If the USART does not support the Receiver timeout feature, this bit is reserved and forced by hardware to ‘0’.
            recv_timeout = USART_ISR_RTOF,

            // CTS:CTS flag
            // This bit is set/reset by hardware. It is an inverted copy of the status of the CTS input pin.
            // 0: CTS line set
            // 1: CTS line reset
            // Note: If the hardware flow control feature is not supported, this bit is reserved and forced by hardware to ‘0’.
            cts = USART_ISR_CTS,

            // CTSIF:CTS interrupt flag
            // This bit is set by hardware when the CTS input toggles, if the CTSE bit is set. It is cleared by software, by writing 1 to the CTSCF bit in the USART_ICR register.
            // An interrupt is generated if CTSIE=1 in the USART_CR3 register.
            // 0: No change occurred on the CTS status line
            // 1: A change occurred on the CTS status line
            // Note: If the hardware flow control feature is not supported, this bit is reserved and forced by hardware to ‘0’.
            cts_intr = USART_ISR_CTSIF,

            // TXE:Transmit data register empty
            // This bit is set by hardware when the content of the USART_TDR register has been transferred into the shift register. It is cleared by a write to the USART_TDR register. An interrupt is generated if the TXEIE bit =1 in the USART_CR1 register.
            // 0: data is not transferred to the shift register
            // 1: data is transferred to the shift register)
            // Note: This bit is used during single buffer transmission.
            tx_empty = USART_ISR_TXE,

            // TC:Transmission complete
            // This bit is set by hardware if the transmission of a frame containing data is complete and if TXE is set. An interrupt is generated if TCIE=1 in the USART_CR1 register. It is cleared by software, writing 1 to the TCCF in the USART_ICR register or by a write to the USART_TDR register.
            // An interrupt is generated if TCIE=1 in the USART_CR1 register.
            // 0: Transmission is not complete
            // 1: Transmission is complete
            // Note: If TE bit is reset and no transmission is on going, the TC bit will be set immediately.
            tx_complete = USART_ISR_TC,

            // RXNE:Read data register not empty
            // This bit is set by hardware when the content of the RDR shift register has been transferred
            // to the USART_RDR register. It is cleared by a read to the USART_RDR register. The RXNE flag can also be cleared by writing 1 to the RXFRQ in the USART_RQR register.
            // An interrupt is generated if RXNEIE=1 in the USART_CR1 register.
            // 0: data is not received
            // 1: Received data is ready to be read.
            recv_not_empty = USART_ISR_RXNE,

            // IDLE:Idle line detected
            // This bit is set by hardware when an Idle Line is detected. An interrupt is generated if IDLEIE=1 in the USART_CR1 register. It is cleared by software, writing 1 to the IDLECF in the USART_ICR register.
            // 0: No Idle line is detected
            // 1: Idle line is detected
            // Note: The IDLE bit will not be set again until the RXNE bit has been set (i.e. a new idle line occurs).
            // If mute mode is enabled (MME=1), IDLE is set if the USART is not mute (RWU=0), whatever the mute mode selected by the WAKE bit. If RWU=1, IDLE is not set.
            idle = USART_ISR_IDLE,

            // ORE:Overrunerror
            // This bit is set by hardware when the data currently being received in the shift register is ready to be transferred into the RDR register while RXNE=1. It is cleared by a software, writing 1 to the ORECF, in the USART_ICR register.
            // An interrupt is generated if RXNEIE=1 or EIE = 1 in the USART_CR1 register.
            // 0: No overrun error
            // 1: Overrun error is detected
            // Note:
            // When this bit is set, the RDR register content is not lost but the shift register is overwritten. An interrupt is generated if the ORE flag is set during multibuffer communication if the EIE bit is set.
            // This bit is permanently forced to 0 (no overrun detection) when the OVRDIS bit is set in the USART_CR3 register.
            overrun_err = USART_ISR_ORE,

            // NF:STARTbit Noise detection flag
            // This bit is set by hardware when noise is detected on a received frame. It is cleared by software, writing 1 to the NFCF bit in the USART_ICR register.
            // 0: No noise is detected
            // 1: Noise is detected
            // Note: Note:
            // This bit does not generate an interrupt as it appears at the same time as the RXNE bit which itself generates an interrupt. An interrupt is generated when the NF flag is set during multibuffer communication if the EIE bit is set.
            // When the line is noise-free, the NF flag can be disabled by programming the ONEBIT bit to 1 to increase the USART tolerance to deviations (Refer to Section 23.4.5: Tolerance of the USART receiver to clock deviation on page 611).
            noise = 0x04,
            // FE:Framingerror
            // This bit is set by hardware when a de-synchronization, excessive noise or a break character is detected. It is cleared by software, writing 1 to the FECF bit in the USART_ICR register. An interrupt is generated if EIE = 1 in the USART_CR1 register.
            // 0: No Framing error is detected
            // 1: Framing error or break character is detected
            framing_err = USART_ISR_FE,

            // PE:Parityerror
            // This bit is set by hardware when a parity error occurs in receiver mode. It is cleared by software, writing 1 to the PECF in the USART_ICR register. 
            // An interrupt is generated if PEIE = 1 in the USART_CR1 register.
            // 0: No parity error
            // 1: Parity error
            parity_err = USART_ISR_PE,
        };
        // Interrupt and status register (USART_ISR)
        bool status(isr f) { return (base->ISR & (uint32_t)f) != 0; }

        // Interrupt flag clear register (USART_ICR)
		enum class icr : uint32_t {
			cmcf = USART_ICR_CMCF, rtocf = USART_ICR_RTOCF, ctscf = USART_ICR_CTSCF,
			tccf = USART_ICR_TCCF, idlecf = USART_ICR_IDLECF, orecf = USART_ICR_ORECF,
			ncf = USART_ICR_NCF, fecf = USART_ICR_FECF, pecf = USART_ICR_PECF
		};
        bool clear(icr f) { return (base->ICR & (uint32_t)f) != 0; }

        // Receive data register (USART_RDR)
        uint8_t receive() { return *(__IO uint8_t*)&base->RDR; }
        void transmit(uint8_t value) { *(__IO uint8_t*)&base->TDR = value; }
    };
    ENUM_FLAGS(usart::intr)
    ENUM_FLAGS(usart::isr)
    ENUM_FLAGS(usart::icr)
}

inline void outstring(char const* str)
{
    using namespace p;
    usart u{USART1};
    while (*str)
    {
        char c = *str++;
        
        while (!u.status(usart::isr::tx_empty | usart::isr::recv_timeout))
            ;

        if (u.status(usart::isr::recv_timeout))
            break;

        u.transmit(c);
    }
}

}

#endif
