#include "utils.h"
#include "config.h"
#include "printf.h"

void init_utils(unsigned timerFlags)
{
	if (timerFlags & utils_ms_timer) {
		TIM16->ARR = 0xffff;
		TIM16->CR1 = TIM16->CR1
			& ~TIM_CR1_CKD_Msk & ~TIM_CR1_CEN
			| BV(TIM_CR1_CKD, 0) | BV(TIM_CR1_CEN, 1);
		TIM16->PSC = CLOCK_MHZ * 1000-1;
	}

	if (timerFlags & utils_us_timer) {
		TIM17->ARR = 0xffff;
		TIM17->CR1 = TIM17->CR1
			& ~TIM_CR1_CKD_Msk & ~TIM_CR1_CEN
			| BV(TIM_CR1_CKD, 0) | TIM_CR1_CEN;
		TIM17->PSC = CLOCK_MHZ-1;
	}
}

void delay_ms(unsigned ms)
{
	uint16_t start = TIM16->CNT;
	while (TIM16->CNT - start < ms)
		;
}

void signal_blink(unsigned count, unsigned delay)
{
//	int delay1 = 700;
	static const int PIN = 4;

	for(int i=0; i<count; ++i)
	{
		GPIOA->BSRR = 0x1 << (16 + PIN);
		delay_ms(delay);

		GPIOA->BSRR = 1 << PIN;
		delay_ms(delay);
	}

//	GPIOA->BSRR = 0x1 << PIN;
	// delay_ms(delay1);
}

void infi_blink(unsigned count, unsigned delay)
{
	while(1)
		signal_blink(count, delay);
}

void report_assert_failed(char const* expr, int line, char const* file)
{
    __disable_irq();
    dbg_printf(col_red "%s:%d: assertion failed: %s\r\n" col_x, file, line, expr);
    __enable_irq();

	infi_blink(4, 200);
}

// 0011 1110
// static uint8_t isspace_[16] = { 0,0x3e,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }; // lsb first bit
// int isspace1(int c) { return c <= 127 ? (isspace_[(c&0x7f) >> 3] & (1 << (c&0x3))) : 0; }
//int isspace(int c) { return c == 32 || (c>=9 && c<=13); }


void* memset(void* p, int value, size_t count)
{
	for (uint8_t* d = p; count --> 0; )
		*d++ = value;
	return p;
}
