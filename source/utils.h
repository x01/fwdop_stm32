#ifndef INC_UTILS_FSwhWhdK_H_
#define INC_UTILS_FSwhWhdK_H_

#include <stddef.h>
#include "macrodef.h"
#include "config.h"

BEGIN_EXTERN_C_FOR_CPP

#ifdef __arm__

enum { utils_ms_timer = 1, utils_us_timer = 2 };
void init_utils(unsigned timerFlags);
void delay_ms(unsigned ms);
inline uint16_t now_ms() { assert(TIM16->CR1&TIM_CR1_CEN); return TIM16->CNT; }
inline uint16_t now_us() { assert(TIM17->CR1&TIM_CR1_CEN); return TIM17->CNT; }
#endif

void signal_blink(unsigned count, unsigned delay);
void infi_blink(unsigned count, unsigned delay);

// "local" stdlib
inline int isspace(int c) { return c == 32 || (c>=9 && c<=13); }
void* memset(void* p, int value, size_t count);

END_EXTERN_C_FOR_CPP

#endif
