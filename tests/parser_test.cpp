#include "../source/parser.h"

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest/doctest.h>
#include <string_view>

#include <cstdio>

using namespace std;

TEST_CASE("parser")
{
	char const* s1 = "+COPS: 00,10,-24,\"AirTel\"";
	int id1, id2, id3;
	str_len sv;
	char const* e1 = parse(s1, strlen(s1), "+COPS:%d, %d, %d,%qs", &id1, &id2, &id3, &sv);

	CHECK(id1 == 0);
	CHECK(id2 == 10);
	CHECK(id3 == -24);
	CHECK(sv.len == 6);
	CHECK(sv.str == s1+18);
	if (sv.str)
		CHECK(std::string(sv.str, sv.len) == "AirTel");
}
