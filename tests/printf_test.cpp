#include <cstdio>
#include <cassert>
#include <cstring>

#include "../source/printf.h"

#include <doctest/doctest.h>

using namespace std;

TEST_CASE("printf")
{
    char buf[32];

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-zero-length"
    SUBCASE("direct") {
        size_t s0 = snprintf(buf, sizeof(buf), "");
        CHECK(s0 == 0);
        CHECK(strcmp(buf, "") == 0);

        size_t s1 = snprintf(buf, sizeof(buf), "a");
        CHECK(s1 == 1);
        CHECK(strcmp(buf, "a") == 0);
    }

#pragma GCC diagnostic ignored "-Wformat"
    SUBCASE("special") {
        size_t s0 = snprintf(buf, sizeof(buf), "%", 0);
        CHECK(s0 == 0);
        CHECK(strcmp(buf, "") == 0);

        size_t s1 = snprintf(buf, sizeof(buf), "%%", 1);
        CHECK(s1 == 1);
        CHECK(strcmp(buf, "%") == 0);
    }
#pragma GCC diagnostic pop

    SUBCASE("int") {
        size_t s0 = snprintf(buf, sizeof(buf), "%d", 0);
        CHECK(s0 == 1);
        CHECK(strcmp(buf, "0") == 0);

        size_t s1 = snprintf(buf, sizeof(buf), "%d", 1);
        CHECK(s1 == 1);
        CHECK(strcmp(buf, "1") == 0);

        size_t s2 = snprintf(buf, sizeof(buf), "%d", -10);
        CHECK(s2 == 3);
        CHECK(strcmp(buf, "-10") == 0);

        size_t s3 = snprintf(buf, sizeof(buf), "%d", 100);
        CHECK(s3 == 3);
        CHECK(strcmp(buf, "100") == 0);
    }

    SUBCASE("int with length") {
        size_t s0 = snprintf(buf, sizeof(buf), "%1d", 0);
        CHECK(s0 == 1);
        CHECK(strcmp(buf, "0") == 0);

        size_t s1 = snprintf(buf, sizeof(buf), "%5d", 1);
        CHECK(s1 == 5);
        CHECK(strcmp(buf, "    1") == 0);

        size_t s2 = snprintf(buf, sizeof(buf), "%11d", -1);
        CHECK(s2 == 11);
        CHECK(strcmp(buf, "         -1") == 0);

        size_t s3 = snprintf(buf, sizeof(buf), "%2d", -100);
        CHECK(s3 == 4);
        CHECK(strcmp(buf, "-100") == 0);
    }

    SUBCASE("int with fill") {
        size_t s0 = snprintf(buf, sizeof(buf), "%01d", 0);
        CHECK(s0 == 1);
        CHECK(strcmp(buf, "0") == 0);

        size_t s1 = snprintf(buf, sizeof(buf), "%05d", 1);
        CHECK(s1 == 5);
        CHECK(strcmp(buf, "00001") == 0);

        size_t s2 = snprintf(buf, sizeof(buf), "%11d", -1);
        CHECK(s2 == 11);
        CHECK(strcmp(buf, "         -1") == 0);

        size_t s3 = snprintf(buf, sizeof(buf), "%011d", -1);
        CHECK(s3 == 11);
        CHECK(strcmp(buf, "-0000000001") == 0);

        size_t s4 = snprintf(buf, sizeof(buf), "%02d", -100);
        CHECK(s4 == 4);
        CHECK(strcmp(buf, "-100") == 0);
    }

    SUBCASE("string") {
        size_t s0 = snprintf(buf, sizeof(buf), "%s", "");
        CHECK(s0 == 0);
        CHECK(strcmp(buf, "") == 0);

        size_t s1 = snprintf(buf, sizeof(buf), "%s", "a");
        CHECK(s1 == 1);
        CHECK(strcmp(buf, "a") == 0);

        size_t s2 = snprintf(buf, sizeof(buf), "%3s", "a");
        CHECK(s2 == 3);
        CHECK(strcmp(buf, "  a") == 0);

        size_t s3 = snprintf(buf, sizeof(buf), "%3s", "bbbb");
        CHECK(s3 == 4);
        CHECK(strcmp(buf, "bbbb") == 0);

        size_t s4 = snprintf(buf, sizeof(buf), "%sb%s", "a", "c");
        CHECK(s4 == 3);
        CHECK(strcmp(buf, "abc") == 0);
    }

    SUBCASE("string with precision") {
        size_t s0 = snprintf(buf, sizeof(buf), "%.2s", "abc");
        CHECK(s0 == 2);
        CHECK(strcmp(buf, "ab") == 0);

        size_t s1 = snprintf(buf, sizeof(buf), "%4.2s", "abc");
        CHECK(s1 == 4);
        CHECK(strcmp(buf, "  ab") == 0);

        size_t s2 = snprintf(buf, sizeof(buf), "%.*s", 3, "abcd");
        CHECK(s2 == 3);
        CHECK(strcmp(buf, "abc") == 0);

        size_t s3 = snprintf(buf, sizeof(buf), "%3.*s", 1, "abc");
        CHECK(s3 == 3);
        CHECK(strcmp(buf, "  a") == 0);

        size_t s4 = snprintf(buf, sizeof(buf), "%4.8s", "abc");
        CHECK(s4 == 4);
        CHECK(strcmp(buf, " abc") == 0);
    }
}