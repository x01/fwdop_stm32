#include <cstdio>
#include <cassert>

#include "../source/ring_buffer.h"

#include <doctest/doctest.h>
#include <string_view>


using namespace std;

TEST_CASE("ring_buffer")
{
    ring_buffer<char, 1> b1;

    CHECK(b1.empty());
    CHECK(b1.capacity() == 1);
    CHECK(b1.size() == 0);
    CHECK(!b1.full());

    b1.push_back('a');
    CHECK(!b1.empty());
    CHECK(b1.front() == 'a');
    CHECK(b1.back() == 'a');
    CHECK(b1.full());
    b1.pop_front();
    CHECK(b1.empty());

    ring_buffer<char, 2> b2;
    b2.push_back('a');
    CHECK(!b2.full());
    CHECK(b2.size() == 1);
    b2.push_back('b');
    CHECK(!b2.empty());
    CHECK(b2.front() == 'a');
    CHECK(b2.back() == 'b');
    CHECK(b2.full());
    CHECK(b2.size() == 2);

    SUBCASE("cctor work") {
        ring_buffer<char, 2> b3 = b2;
        CHECK(b3.front() == 'a');
        CHECK(b3.back() == 'b');
    }
    SUBCASE("operator= work") {
        ring_buffer<char, 2> b4;
        b4 = b2;
        CHECK(b4.front() == 'a');
        CHECK(b4.back() == 'b');
    }


    b2.pop_front();
    CHECK(!b2.empty());
    CHECK(b2.size() == 1);
    b2.pop_back();
    CHECK(b2.empty());
}