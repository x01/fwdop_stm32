// clang++ -O2 makeimg.cpp && ./a.out
#include <fstream>
#include <vector>

#include "scroll_image_gray.c"

bool get_bit(unsigned x, unsigned y)
{
    unsigned w = gimp_image.width, h = gimp_image.height;
    unsigned bpp = gimp_image.bytes_per_pixel;
    uint8_t const* src = (uint8_t const*)gimp_image.pixel_data;

    unsigned o = (y * w + x) * bpp;
    return src[o+0] >= 0x80;
}

int main()
{
    std::vector<uint8_t> p;

    unsigned w = gimp_image.width, h = gimp_image.height;
    unsigned bpp = gimp_image.bytes_per_pixel;
    uint8_t const* src = (uint8_t const*)gimp_image.pixel_data;

    p.resize(w/8*h);

    for (int y=0; y<h; ++y)
    {
        for(int x=0; x<w; x+=8)
        {
            uint8_t v = 0;
            for (int i=0; i<8; ++i) {
                v <<= 1;
                v |= get_bit(x + i, y) ? 0x1 : 0;
            }
            p[(x/8)*h + y] = v;
        }
    }

    std::ofstream f("scroll_image_bw.c", std::ios_base::out);
    f << "uint8_t const static_image_data[] = {\n";
    for (int i=0; i<p.size(); i+=16) {
        for(int q=0; q<16; ++q)
            f << (int)p[i+q] << ",";
        f << "\n";
    }
    f << "};";
    f << "struct { uint16_t w, h; uint8_t const* p; } static_image = {\n    " << w << ", " << h << ", static_image_data };\n";
    
    f.close();

    return 0;
}